cmake_minimum_required(VERSION 3.16)
project(Cake)

# set(ENV{ASM_NASM} nasm)
# enable_language(ASM_NASM)

include(CTest)

# find_package(Vulkan REQUIRED)
set(Vulkan_INCLUDE_DIRS $ENV{VULKAN_SDK}/include)
set(Vulkan_LIBRARIES $ENV{VULKAN_SDK}/lib/vulkan-1.lib)

add_compile_definitions(_CRT_SECURE_NO_WARNINGS)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug)
endif()

if(CMAKE_BUILD_TYPE STREQUAL Debug)
    add_compile_definitions(
        CAKE_DEBUG_MEMORY
        # CAKE_DEBUG_ENABLE_LOGGING
        CAKE_DEBUG_ASSERT
        CAKE_DEBUG_BREAK
        CAKE_DEBUG_ENABLE_INFO
    )

    message(NOTICE "Setting up a debug build...")
endif()

string(APPEND CMAKE_CXX_FLAGS "-g0")

if(${WIN32})
    add_compile_definitions(NOMINMAX)
endif()

add_subdirectory(src/tests)
add_subdirectory(src/cake)
add_subdirectory(src/samples)
add_subdirectory(src/benchmarks)
