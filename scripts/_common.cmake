get_filename_component(PROJECT_DIR ${CMAKE_CURRENT_LIST_DIR} DIRECTORY)
set(OUT_DIR ${PROJECT_DIR}/out)
set(BUILD_DIR ${PROJECT_DIR}/build)

set(COMPILE_COMMANDS_FILE ${BUILD_DIR}/compile_commands.json)
