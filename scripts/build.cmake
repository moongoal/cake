include(${CMAKE_CURRENT_LIST_DIR}/_common.cmake)

execute_process(
    COMMAND
        ${CMAKE_COMMAND}
        --build
        ${BUILD_DIR}
    WORKING_DIRECTORY ${PROJECT_DIR}
    COMMAND_ECHO STDOUT
)
