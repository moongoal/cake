include(${CMAKE_CURRENT_LIST_DIR}/_common.cmake)

find_program(
    EXE_CLANG
    clang
)

if(${EXE_CLANG} STREQUAL "EXE_CLANG-NOTFOUND")
    message(FATAL_ERROR Unable to find clang)
endif()

execute_process(
    COMMAND
        ${CMAKE_COMMAND}
        -Wdev
        -G Ninja
        -S ${PROJECT_DIR}
        -B ${BUILD_DIR}
        -DCMAKE_EXPORT_COMPILE_COMMANDS=1
        -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=${OUT_DIR}
        -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=${OUT_DIR}
        -DCMAKE_C_COMPILER:PATH=${EXE_CLANG}
        -DCMAKE_CXX_COMPILER:PATH=${EXE_CLANG}
        -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
    WORKING_DIRECTORY ${PROJECT_DIR}
    COMMAND_ECHO STDOUT
)

# Copy compile commands file for clangd
execute_process(
    COMMAND
    ${CMAKE_COMMAND}
    -E copy_if_different
    ${COMPILE_COMMANDS_FILE}
    ${PROJECT_DIR}
)
