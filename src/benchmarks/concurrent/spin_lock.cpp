#include <algorithm>
#include <cstdint>
#include <limits>
#include <numeric>
#include <chrono>
#include <vector>
#include <thread>
#include <iostream>
#include <ostream>
#include <testfw/benchmark.hpp>
#include <cake/concurrent/thread.hpp>
#include <cake/concurrent/spin_lock.hpp>

using namespace cake::concurrent;

static unsigned long long constexpr n_iterations = 200;
static unsigned long long constexpr min_hold_time = 10;
static unsigned long long constexpr max_hold_time = 100;
static unsigned long long constexpr hold_time_inc = 10; // Multiplicative increment factor
static unsigned long long constexpr max_threads = 32;
static unsigned long long constexpr max_threads_inc = 2; // Multiplicative increment factor

struct record {
	static constexpr size_t bench_n_entries = max_threads;

	alignas(std::max(
		std::hardware_destructive_interference_size,
		alignof(
			double))) double waiting_time[bench_n_entries]; // Acquire waiting time of each thread

	constexpr record(): waiting_time { 0 } {}

	constexpr void clean()
	{
		for (unsigned long long i = 0; i < max_threads; ++i) { waiting_time[i] = 0.0; }
	}
};

std::ostream &operator<<(std::ostream &os, record &record)
{
	for (unsigned long long i = 0; i < max_threads; ++i) {
		os << record.waiting_time[i];

		if (i < max_threads - 1) { os << ','; }
	}
	return os;
}

spin_lock lock;

void sl_thread(unsigned long long const hold_time, volatile double &time_result) noexcept
{
	auto t_begin = std::chrono::high_resolution_clock::now();
	lock.acquire();
	auto t_acquired = std::chrono::high_resolution_clock::now();
	sleep(hold_time);
	lock.release();

	time_result = ((std::chrono::duration<double>)(t_acquired - t_begin)).count();
}

BENCHMARK
{
	auto results = testfw::benchmark::benchmark_result<
		unsigned long long, /* hold_time */
		unsigned long long, /* n_threads */
		unsigned long long, /* iteration */
		record &			/* waiting time */
		> { "spin_lock.csv" };

	results.set_labels("hold_time", "n_threads", "iteration", "wait_time");

	for (unsigned long long hold_time = min_hold_time; hold_time <= max_hold_time;
		 hold_time *= hold_time_inc) {
		for (unsigned long long n_threads = 1; n_threads <= max_threads;
			 n_threads *= max_threads_inc) {
			std::cout << "Hold: " << hold_time << "/" << max_hold_time << "; Threads: " << n_threads
					  << "/" << max_threads << std::endl;

			for (unsigned long long i = 0; i < n_iterations; ++i) {
				record rec;
				std::vector<std::thread> threads;

				threads.reserve(n_threads);

				// Run threads
				for (unsigned tnum = 0; tnum < n_threads; ++tnum) {
					threads.push_back(
						std::thread { sl_thread, hold_time, std::ref(rec.waiting_time[tnum]) });
				}

				std::for_each(threads.begin(), threads.end(), [](std::thread &t) { t.join(); });

				// Write record
				results.add_results(hold_time, n_threads, i, rec);
			}
		}
	}
}
