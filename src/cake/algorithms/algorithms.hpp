#ifndef CAKE_ALGORITHMS_H
#define CAKE_ALGORITHMS_H

namespace cake::algorithms {
	template<typename T>
	T *find_next(T *const ptr_start, T *const ptr_end, T const target) {
		T *ptr;

		for (ptr = ptr_start; ptr != ptr_end; ++ptr) {
			if (*ptr == target) { break; }
		}

		return ptr;
	}

	template<typename T>
	T *find_last(T *const ptr_start, T *const ptr_end, T const target) {
		T *prev_ptr = nullptr;

		for (T *ptr = ptr_start; ptr != ptr_end; ++ptr) {
			ptr = find_next(ptr, ptr_end, target);

			if (ptr == ptr_end) { break; }

			prev_ptr = ptr;
		}

		return prev_ptr;
	}
}

#endif // CAKE_ALGORITHMS_H
