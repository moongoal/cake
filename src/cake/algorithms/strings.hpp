#ifndef CAKE_ALGORITHMS_STRINGS_H
#define CAKE_ALGORITHMS_STRINGS_H

#include <cstdio>
#include <cstdlib>
#include <cake/types.hpp>

namespace cake::algorithms {
	template<typename... Args>
	char *
	value_to_str(char *const buff, size_type const buff_len, const char *const value, Args... args) {
		buff[0] = '\0';

		::strcat(buff, value);
		size_type str_len = ::strlen(value);

		if constexpr(sizeof...(args) > 0) {
			value_to_str(buff + str_len, buff_len - str_len, args...);
		}

		return buff;
	}

    template<typename T, typename... Args>
	char *value_to_str(char *const buff, size_type const buff_len, T value, Args... args) {
		static_assert(std::is_integral<T>::value, "Type not supported");

		buff[0] = '\0';
		
		if constexpr(std::is_integral<T>::value) {
			if constexpr(std::is_unsigned<T>::value) {
				::sprintf(buff, "%llu", (unsigned long long)value);
			} else {
				::sprintf(buff, "%lld", (long long)value);
			}
		}
		
        size_type str_len = ::strlen(buff);
		
		if constexpr(sizeof...(args) > 0) {
			value_to_str(buff + str_len, buff_len - str_len, args...);
		}

		return buff;
	}
}

#endif // CAKE_ALGORITHMS_STRINGS_H
