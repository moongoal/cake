#ifndef CAKE_API_H
#define CAKE_API_H

#ifdef CAKE_API
    #define CAKE_API_EXPORT __declspec(dllexport)
#else // CAKE_API
    #define CAKE_API_EXPORT __declspec(dllimport)
#endif // CAKE_API

#define CAKE_INLINE __attribute__((always_inline))

#include "types.hpp"

#endif // CAKE_API_H
