#ifndef CAKE_COLLECTIONS_QUEUE_H
#define CAKE_COLLECTIONS_QUEUE_H

#include <utility>
#include <memory>
#include <cake/types.hpp>
#include <cake/memory.hpp>
#include "simple_iterator.hpp"

namespace cake::collections {
	/**
	 * Single-ended queue.
	 *
	 * @tparam T The item type
	 * @tparam Allocator The allocator type
	 */
	template<typename T, typename Allocator = cake::memory::host::allocator<T>>
	class queue {
	  public:
		using allocator_type = Allocator;
		using allocator_traits = cake::memory::host::allocator_traits<allocator_type>;
		using value_type = T;
		using pointer = T *;
		using reference = T &;
		using const_reference = const T &;
		using iterator = simple_iterator<T>;
		using size_type = cake::size_type;

	  private:
		pointer mutable m_memory;	/**< Pointer to collection's memory */
		pointer m_col_start;		/**< Pointer to beginning of collection (first stored item) */
		pointer m_col_end;			/**< Pointer to end of collection (after last stored item) */
		size_type m_capacity;		/**< Collection's capacity */
		allocator_type m_allocator; /**< Memory allocator */

		/**
		 * Ensure the capacity is at least `capacity`.
		 *
		 * @param capacity The capacity to ensure is available
		 */
		void ensure_capacity(size_type capacity) noexcept
		{
			if (capacity < m_capacity) { reserve(capacity); }
		}

		/**
		 * Ensure that enqueueing is possible.
		 */
		void ensure_enqueue_possible() noexcept
		{
			ensure_capacity(size() + 1);
			reset();
		}

	  public:
		/**
		 * Initialize a new instance of this class.
		 *
		 * @param initial_capacity The initial capacity
		 * @param allocator The item's memory allocator
		 */
		explicit queue(
			size_type initial_capacity,
			const allocator_type &allocator = allocator_type {}) noexcept:
			m_memory { allocator_traits::allocate(
				const_cast<allocator_type &>(allocator),
				initial_capacity) },
			m_col_start { m_memory },
			m_col_end { m_memory },
			m_capacity { initial_capacity },
			m_allocator { allocator_traits::select_on_container_copy_construction(allocator) }
		{
		}

		/**
		 * Initialize a new instance of this class.
		 *
		 * @param other The object to copy the initial state from
		 */
		queue(const queue &other) noexcept:
			m_memory {
				allocator_traits::allocate(const_cast<queue &>(other).m_allocator, other.m_capacity)
			},
			m_col_start { m_memory },
			m_col_end { m_memory },
			m_capacity { other.m_capacity },
			m_allocator { allocator_traits::select_on_container_copy_construction(
				other.m_allocator) }
		{
			const iterator it_end = other.cend();

			for (iterator it = other.begin(); it != it_end; ++it) { enqueue_unchecked(*it); }
		}

		/**
		 * Initialize a new instance of this class.
		 *
		 * @param other The object to copy the initial move from
		 */
		queue(queue &&other) noexcept:
			m_memory { std::move(other.m_memory) },
			m_col_start { std::move(other.m_col_start) },
			m_col_end { std::move(other.m_col_end) },
			m_capacity { std::move(other.m_capacity) },
			m_allocator { std::move(other.m_allocator) }
		{
		}

		~queue() noexcept { allocator_traits::deallocate(m_allocator, m_memory, m_capacity); }

		/**
		 * Enqueue an item.
		 *
		 * @param item The item to enqueue
		 */
		void enqueue(const_reference item) noexcept
		{
			ensure_enqueue_possible();
			enqueue_unchecked(item);
		}

		/**
		 * Enqueue an item.
		 *
		 * @param item The item to enqueue
		 */
		void enqueue(value_type &&item) noexcept
		{
			ensure_enqueue_possible();

			enqueue_unchecked(std::move(item));
		}

		/**
		 * Enqueue an item without doing capacity checks. Faster but dangerous.
		 *
		 * @param item The item to enqueue
		 */
		void enqueue_unchecked(const_reference item) noexcept { new (m_col_end++) T(item); }

		/**
		 * Enqueue an item without doing capacity checks. Faster but dangerous.
		 *
		 * @param item The item to enqueue
		 */
		void enqueue_unchecked(value_type &&item) noexcept { new (m_col_end++) T(std::move(item)); }

		/**
		 * Dequeue an item.
		 *
		 * @return The item removed from the start of the queue.
		 */
		value_type dequeue() noexcept
		{
			cake_assert(size() > 0);
			value_type value = T(std::move(*m_col_start));

			m_col_start->~T();
			m_col_start++;

			return std::move(value);
		}

		/**
		 * Peek at the start of the queue.
		 *
		 * @return A reference to the next element to be dequeued.
		 */
		[[nodiscard]] reference peek() const noexcept
		{
			cake_assert(size() > 0);

			return *m_col_start;
		}

		/**
		 * Reserve some memory.
		 *
		 * @param capacity The minimum number of items to reserve memory for.
		 */
		void reserve(size_type capacity) noexcept
		{
			T *const new_memory =
				allocator_traits::reallocate(m_allocator, m_memory, m_capacity, capacity);

			m_col_start = new_memory + (m_col_start - m_memory);
			m_col_end = new_memory + (m_col_end - m_memory);
			m_capacity = capacity;
			m_memory = new_memory;
		}

		/**
		 * Reset the structure to its optimal state to ensure faster enqueueing operations.
		 * Called automatically by `enqueue()`.
		 */
		void reset() noexcept
		{
			if (is_empty()) {
				m_col_start = m_col_end = m_memory;
			} else if (m_col_end >= m_memory + m_capacity) {
				pointer ptr = m_memory;
				size_type const sz = size();
				const iterator end = cend();

				for (iterator it = begin(); it < end; ++it) {
					new (ptr) T(std::move(*it));
					ptr->~T();
					++ptr;
				}

				m_col_start = m_memory;
				m_col_end = m_memory + sz;
			}
		}

		/**
		 * Return the size of the collection.
		 *
		 * @return The number of currently contained items
		 */
		[[nodiscard]] size_type size() const noexcept { return m_col_end - m_col_start; }

		/**
		 * Return whether the collection is empty.
		 *
		 * @return True if the collection is empty, false if not.
		 */
		[[nodiscard]] bool is_empty() const noexcept { return !size(); }

		/**
		 * Return an iterator to the beginning of the collection.
		 *
		 * @return An iterator to the first element of the collection.
		 */
		[[nodiscard]] iterator begin() const noexcept { return std::move(iterator(m_col_start)); }

		/**
		 * Return an iterator to the end of the collection.
		 *
		 * @return An iterator pointing past the last element of the collection.
		 */
		[[nodiscard]] iterator end() const noexcept { return std::move(iterator(m_col_end)); }

		/**
		 * Return an iterator to the beginning of the collection.
		 *
		 * @return An iterator to the first element of the collection.
		 */
		[[nodiscard]] const iterator cbegin() const noexcept
		{
			return std::move(iterator(m_col_start));
		}

		/**
		 * Return an iterator to the end of the collection.
		 *
		 * @return An iterator pointing past the last element of the collection.
		 */
		[[nodiscard]] const iterator cend() const noexcept
		{
			return std::move(iterator(m_col_end));
		}
	};
}

#endif // CAKE_COLLECTIONS_QUEUE_H
