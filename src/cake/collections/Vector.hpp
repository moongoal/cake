#ifndef CAKE_COLLECTIONS_VECTOR_H
#define CAKE_COLLECTIONS_VECTOR_H

#include <cstring>
#include <malloc.h>
#include <utility>
#include <cake/types.hpp>
#include <cake/memory.hpp>
#include "simple_iterator.hpp"

namespace cake::collections {
	/**
	 * Generic continuous linear data structure.
	 *
	 * @tparam T The type of element contained in the vector
	 * @tparam Allocator The allocator type
	 */
	template<typename T, typename Allocator = cake::memory::host::allocator<T>>
	class vector {
	  public:
		using allocator_type = Allocator;
		using allocator_traits = cake::memory::host::allocator_traits<allocator_type>;
		using value_type = T;
		using reference = T &;
		using const_reference = const T &;
		using pointer = T *;
		using const_pointer = const T &;
		using iterator = simple_iterator<T>;
		using const_iterator = const simple_iterator<T>;

	  private:
		pointer m_data_ptr;			/**< Pointer to the vector data */
		size_type m_size;			/**< Size of the vector */
		size_type m_capacity;		/**< Capacity of the vector */
		allocator_type m_allocator; /**< Memory allocator */

		/**
		 * Ensure a minimum capacity for the container is met.
		 * If the requirement is not satisfied, reserve() is called.
		 *
		 * @param min_capacity The minimum capacity the container must have
		 */
		void ensure_capacity(size_type min_capacity) noexcept
		{
			if (m_capacity < min_capacity) { reserve(m_capacity * 2); }
		}

	  public:
		/**
		 * Initialize a new instance of this class.
		 *
		 * @param initial_capacity The initial capacity of the container
		 * @param allocator The item's memory allocator
		 */
		explicit vector(
			size_type initial_capacity,
			const allocator_type &allocator = allocator_type {}) noexcept:
			m_data_ptr { allocator_traits::allocate(
				const_cast<allocator_type &>(allocator),
				initial_capacity) },
			m_size { 0 },
			m_capacity { initial_capacity },
			m_allocator { allocator_traits::select_on_container_copy_construction(allocator) }
		{
		}

		/**
		 * Initialize a new instance of this class.
		 *
		 * @param other The instance to move
		 */
		vector(vector &&other) noexcept:
			m_data_ptr { std::move(other.m_data_ptr) },
			m_size { std::move(other.m_size) },
			m_capacity { std::move(other.m_capacity) },
			m_allocator { std::move(other.m_allocator) }
		{
		}

		/**
		 * Initialize a new instance of this class.
		 *
		 * @param other The instance to copy
		 */
		vector(const vector &other) noexcept:
			m_data_ptr { allocator_traits::allocate(other.m_allocator, other.m_capacity) },
			m_size { other.m_size },
			m_capacity { other.m_capacity },
			m_allocator { allocator_traits::select_on_container_copy_construction(
				other.m_allocator) }
		{
			const iterator it_end = other.cend();

			for (iterator it = other.begin(); it != it_end; ++it) { push_back(*it); }
		}

		virtual ~vector() noexcept
		{
			allocator_traits::deallocate(m_allocator, m_data_ptr, m_capacity);
		}

		allocator_type get_allocator() const noexcept { return allocator_type {}; }

		/**
		 * Reserve memory for the container.
		 *
		 * @param capacity The capacity (in number of items) that the container must reserve.
		 */
		void reserve(size_type capacity) noexcept
		{
			m_data_ptr =
				allocator_traits::reallocate(m_allocator, m_data_ptr, m_capacity, capacity);
			m_size = reinterpret_cast<size_type>(utils::choose(
				reinterpret_cast<uint64_t>(capacity),
				reinterpret_cast<uint64_t>(m_size),
				capacity < m_size));
			m_capacity = capacity;
		}

		/**
		 * Append a value to the back of the container.
		 *
		 * @param value The value to add
		 */
		void push_back(value_type &&value) noexcept
		{
			size_type idx = m_size++;
			ensure_capacity(m_size);

			new (m_data_ptr + idx) value_type(std::move(value));
		}

		/**
		 * Append a value to the back of the container.
		 *
		 * @param value The value to add
		 */
		void push_back(const_reference value) noexcept
		{
			size_type idx = m_size++;
			ensure_capacity(m_size);

			new (m_data_ptr + idx) value_type(value);
		}

		/**
		 * Remove an item from the back of the container.
		 */
		void pop() noexcept
		{
			cake_assert(m_size);

			size_type idx = --m_size;
			value_type *ptr = m_data_ptr + idx;

			ptr->~value_type();
		}

		/**
		 * Get the last item from the container.
		 *
		 * @return The last item
		 */
		reference back() noexcept { return *(m_data_ptr + m_size - 1); }

		/**
		 * Get the last item from the container.
		 *
		 * @return The last item
		 */
		const value_type &back() const noexcept { return *(m_data_ptr + m_size - 1); }

		/**
		 * Get the size of the container.
		 *
		 * @return The size
		 */
		size_type size() const noexcept { return m_size; }

		/**
		 * Get the capacity of the container.
		 *
		 * @return The capacity
		 */
		size_type capacity() const noexcept { return m_capacity; }

		/**
		 * Get the pointer to the data of the container.
		 *
		 * @return The pointer to the data
		 */
		pointer data() noexcept { return m_data_ptr; }

		/**
		 * Access an item given at a given position.
		 *
		 * @param index The index of the item to get
		 *
		 * @return The item at position `index`
		 */
		reference operator[](size_type index) noexcept
		{
			cake_assert(index < m_size);

			return *(m_data_ptr + index);
		}

		/**
		 * Access an item given at a given position.
		 *
		 * @param index The index of the item to get
		 *
		 * @return The item at position `index`
		 */
		const_reference &operator[](size_type index) const noexcept
		{
			cake_assert(index < m_size);

			return *(m_data_ptr + index);
		}

		/**
		 * Remove an item.
		 *
		 * @param index The index of the item to remove
		 */
		void remove(size_type index) noexcept
		{
			cake_assert(index < m_size);

			const_iterator it_end = cend() - 1;
			iterator it = begin() + index;

			(*it).~value_type();

			for (; it != it_end; ++it) { *it = *(it + 1); }

			(*it).~value_type();
			m_size--;
		}

		/**
		 * Resize the container.
		 * The items lost when the container is shrunk are destroyed and those
		 * created when the container is enlarged are initialized by their default constructor.
		 *
		 * @param new_size The new size of the container
		 */
		void resize(size_type new_size) noexcept
		{
			if (m_size < new_size) { // Grow-resize
				if (m_capacity < new_size) { reserve(new_size); }

				for (size_type i = m_size; i < new_size; ++i) { (m_data_ptr + i)->value_type(); }
			} else if (m_size > new_size) {
				for (size_type i = new_size; i < m_size; ++i) { (m_data_ptr + i)->~value_type(); }
			}

			m_size = new_size;
		}

		/**
		 * Insert an item in the container.
		 *
		 * @param index The position where to insert the new item
		 * @param value The value to insert
		 */
		void insert(size_type index, value_type value) noexcept
		{
			cake_assert(index < size()); // Invalid index

			ensure_capacity(size() + 1);

			iterator base = begin() + index;
			iterator prebase = base - 1;

			for (iterator it = end() - 1; it != prebase; --it) {
				value_type *const p = &*it;

				new (p + 1) value_type(std::move(*p));
			}

			new (&*base) value_type(value);
			++m_size;
		}

		/**
		 * Get an iterator pointing to the beginning of the collection.
		 *
		 * @return an iterator set at the first item
		 */
		iterator begin() { return std::move(iterator { data() }); }

		/**
		 * Get an iterator pointing to the end of the collection.
		 *
		 * @return an iterator set past the last item.
		 */
		iterator end() { return std::move(iterator { data() + size() }); }

		/**
		 * Get an iterator pointing to the beginning of the collection.
		 *
		 * @return an iterator set at the first item
		 */
		const_iterator cbegin() { return std::move(iterator { data() }); }

		/**
		 * Get an iterator pointing to the end of the collection.
		 *
		 * @return an iterator set past the last item.
		 */
		const_iterator cend() { return std::move(iterator { data() + size() }); }
	};
}

#endif // CAKE_COLLECTIONS_VECTOR_H
