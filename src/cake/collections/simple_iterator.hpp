#ifndef CAKE_COLLECTIONS_VECTOR_ITERATOR_H
#define CAKE_COLLECTIONS_VECTOR_ITERATOR_H

#include <type_traits>
#include <iterator>
#include <cake/types.hpp>

namespace cake::collections {
	/**
	 * A standard-library-compliant random access iterator for contiguously allocated sequences.
	 */
	template<typename T>
	class simple_iterator {
	  public:
		typedef simple_iterator<T> iterator;
		typedef const simple_iterator<T> const_iterator;
		typedef long long difference_type;
		typedef T value_type;
		typedef T *pointer;
		typedef T &reference;
		typedef const T &const_reference;
		typedef std::random_access_iterator_tag iterator_category;

	  private:
		pointer m_ptr;

	  public:
		simple_iterator() noexcept: m_ptr { 0 } {}

		simple_iterator(pointer const ptr) noexcept: m_ptr { ptr }
		{
			cake_assert(m_ptr); // Must never be nullptr
		}

		simple_iterator(iterator &&other) noexcept: m_ptr { other.m_ptr } {}

		simple_iterator(const_iterator &other) noexcept: m_ptr { other.m_ptr } {}

		~simple_iterator() {}

		iterator &operator=(iterator &&other) noexcept
		{
			m_ptr = other.m_ptr;

			return *this;
		}

		iterator &operator=(const_iterator &other) noexcept
		{
			m_ptr = other.m_ptr;

			return *this;
		}

		reference operator*() noexcept { return *m_ptr; }

		pointer operator->() noexcept { return m_ptr; }

		reference operator[](int index) noexcept { return *(m_ptr + index); }

		iterator &operator++() noexcept
		{
			m_ptr++;

			return *this;
		}

		iterator operator++(int) noexcept
		{
			iterator tmp { *this };
			++m_ptr;

			return std::move(tmp);
		}

		iterator &operator--() noexcept
		{
			m_ptr--;
			return *this;
		}

		iterator operator--(int) noexcept
		{
			iterator tmp { *this };
			--m_ptr;

			return std::move(tmp);
		}

		iterator operator+(int n) const noexcept { return std::move(iterator(m_ptr + n)); }

		iterator operator-(int n) const noexcept { return std::move(iterator(m_ptr - n)); }

		difference_type operator-(const iterator &other) const noexcept
		{
			return m_ptr - other.m_ptr;
		}

		bool operator==(const iterator &other) const noexcept { return m_ptr == other.m_ptr; }

		bool operator!=(const iterator &other) const noexcept { return !(*this == other); }

		bool operator<(const iterator &other) const noexcept { return m_ptr < other.m_ptr; }
		bool operator>(const iterator &other) const noexcept { return m_ptr > other.m_ptr; }
		bool operator<=(const iterator &other) const noexcept { return m_ptr <= other.m_ptr; }
		bool operator>=(const iterator &other) const noexcept { return m_ptr >= other.m_ptr; }
	};
}

#endif // CAKE_COLLECTIONS_VECTOR_ITERATOR_H
