#ifndef CAKE_CONCURRENT_LOCK_H
#define CAKE_CONCURRENT_LOCK_H

#include <limits>

#ifdef _WIN32
	#include <Windows.h>
#else
	#error OS not supported
#endif // _WIN32

#include <cake/api.hpp>
#include <cake/types.hpp>
#include <cake/debug/debug.hpp>

namespace cake::concurrent {
	/**
	 * A lock that busy-waits at first and then yields control until the resource is available.
	 */
	class lock {
		CRITICAL_SECTION m_lock;

	  public:
		/**
		 * Initialize a new instance of this class.
		 *
		 * @param spin_count The number of times to spin before yielding control
		 */
		explicit lock(size_type spin_count = 0) noexcept
		{
			cake_assert(spin_count <= std::numeric_limits<DWORD>::max());

			::InitializeCriticalSectionAndSpinCount(&m_lock, (DWORD)spin_count);
		}

		~lock() noexcept { ::DeleteCriticalSection(&m_lock); }

		/**
		 * Try to acquire the lock and if it's already held, give up immediately.
		 *
		 * @return True if the lock has been acquired, false if it hasn't because some other thread
		 * alraedy holds it
		 */
		CAKE_INLINE bool try_acquire() noexcept { return (bool)::TryEnterCriticalSection(&m_lock); }

		/**
		 * Wait until the lock is available and then acquire it.
		 */
		CAKE_INLINE void acquire() noexcept { ::EnterCriticalSection(&m_lock); }

		/**
		 * Release the lock.
		 */
		CAKE_INLINE void release() noexcept { ::LeaveCriticalSection(&m_lock); }
	};
}

#endif // CAKE_CONCURRENT_LOCK_H
