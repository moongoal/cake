#ifndef CAKE_CONCURRENT_LOCK_GUARD_H
#define CAKE_CONCURRENT_LOCK_GUARD_H

#include <cake/api.hpp>

namespace cake::concurrent {
	/**
	 * A lock guard that ensures a lock is immediately acquired upon guard creation
	 * and released upon guard destruction.
	 *
	 * @tparam T The type of lock
	 */
	template<typename T>
	class lock_guard {
	  public:
		using lock_type = T;		/**< Type of lock held */
		using lock_reference = T &; /**< Reference to lock */

	  private:
		lock_reference m_lock; /**< Held lock */

	  public:
		/**
		 * Initialize a new instance of this class.
		 *
		 * @param lock The lock to guard
		 *
		 * @remarks The specified lock is acquired immediately. If the lock is already held by
		 * another thread, this guard will wait until the lock is available
		 */
		explicit CAKE_INLINE lock_guard(lock_reference lock) noexcept: m_lock { lock }
		{
			m_lock.acquire();
		}

		/**
		 * Destroy an existing instance of this class.
		 *
		 * @remarks The lock is released immediately upon guard destruction
		 */
		CAKE_INLINE ~lock_guard() noexcept { m_lock.release(); }
	};
}

#endif // CAKE_CONCURRENT_LOCK_GUARD_H
