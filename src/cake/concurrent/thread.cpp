#include <atomic>
#include <cake/debug/debug.hpp>
#include <limits>
#include <stdint.h>
#include "thread.hpp"

namespace {
	extern "C" DWORD WINAPI start_new_thread(LPVOID thread_ptr)
	{
		cake::concurrent::thread::start_new_thread(
			*reinterpret_cast<cake::concurrent::thread *>(thread_ptr));

		return (DWORD)0;
	}
}

namespace cake::concurrent {
	constexpr const char *const UNNAMED_THREAD_NAME = "Unnamed thread";

	thread_id get_current_thread_id() noexcept { return GetCurrentThreadId(); }

	void sleep(size_type duration_ms) noexcept
	{
		cake_assert(duration_ms <= std::numeric_limits<DWORD>::max());

		::Sleep((DWORD)duration_ms);
	}

	thread::~thread() noexcept
	{
		cake_assert(m_state.load(std::memory_order_acquire) != thread_state::running);
		thread_id tid = m_id.load(std::memory_order_acquire);

		if (tid != INVALID_THREAD_ID) { CloseHandle(m_win_handle); }
	}

	void thread::start_new_thread(thread &t) noexcept
	{
		thread_id tid = get_current_thread_id();
		cake_assert(tid != INVALID_THREAD_ID);

		t.m_handler(const_cast<void *>(t.m_param));
		t.m_state.store(thread_state::terminated, std::memory_order_release);
	}

	void thread::start(void *param) noexcept
	{
		cake_assert(m_state.load(std::memory_order_acquire) == thread_state::created);

		m_param = param;
		HANDLE thandle = CreateThread(NULL, 0, &::start_new_thread, this, NULL, NULL);
		cake_assert(thandle != NULL);

		m_id.store(GetThreadId(thandle), std::memory_order_release);

		/*
		 * TODO: This information is not immediately available to other threads.
		 * A condition variable to be enforced to ensure all the data is written prior to user code to start.
		 *
		 * NOTE: An alternative might be to make m_win_handle atomic and wait for m_state to be
		 * "running"
		 */
		m_win_handle = thandle;
		m_state.store(thread_state::running, std::memory_order_release);
	}

	void thread::join() noexcept
	{
		while (true) {
			if (try_join(std::numeric_limits<unsigned>::max())) { break; }
		}
	}

	bool thread::try_join(unsigned const timeout_ms) noexcept
	{
		thread_id tid = m_id.load(std::memory_order_acquire);

		cake_assert(tid != get_current_thread_id() && tid != INVALID_THREAD_ID);

		thread_state const state = m_state.load(std::memory_order_acquire);

		if (state == thread_state::terminated) { return true; }

		cake_assert(state == thread_state::running);
		cake_assert(timeout_ms <= std::numeric_limits<DWORD>::max());

		DWORD result = WaitForSingleObject(m_win_handle, (DWORD)timeout_ms);

		cake_assert(result == WAIT_OBJECT_0 || result == WAIT_TIMEOUT);

		return result == WAIT_OBJECT_0;
	}

	unsigned get_available_processor_count() noexcept
	{
		return (unsigned)GetActiveProcessorCount(ALL_PROCESSOR_GROUPS);
	}

	uint64_t get_process_affinity_mask() noexcept
	{
		DWORD_PTR proc_affinity, sys_affinity;

		BOOL result =
			(uint64_t)GetProcessAffinityMask(GetCurrentProcess(), &proc_affinity, &sys_affinity);

		cake_assert(result);

		return (uint64_t)proc_affinity;
	}

	void thread::set_affinity(uint64_t mask) const noexcept
	{
		DWORD_PTR old_affinity_mask = SetThreadAffinityMask(m_win_handle, (DWORD_PTR)mask);

		cake_assert(old_affinity_mask);
	}
}
