#ifndef CAKE_CONCURRENT_THREAD_H
#define CAKE_CONCURRENT_THREAD_H

#include <cstdint>
#include <type_traits>
#include <utility>
#include <atomic>

#ifdef _WIN32
	#include <Windows.h>
#else // _WIN32
	#error Current OS not supported
#endif // _WIN32

#include <cake/api.hpp>
#include <cake/types.hpp>
#include <cake/debug/debug.hpp>

namespace cake::concurrent {
	/** Implementation-specific thread identifier type */
	using thread_id = DWORD;

	/**
	 * Thread handler pointer type.
	 *
	 * This implementation-independent handler type represents
	 * a function, entry-point for a thread. It accepts a parameter that will be passed through the
	 * `thread::start()` member function.
	 */
	using thread_handler_ptr = void (*)(void *param) noexcept;

	/** The invalid thread ID value */
	constexpr thread_id INVALID_THREAD_ID = (
#ifdef _WIN32
		(thread_id)NULL
#endif // _WIN32
	);

	extern CAKE_API_EXPORT const char *const UNNAMED_THREAD_NAME;

	/** Return the thread ID of the currently executing thread. */
	CAKE_API_EXPORT thread_id get_current_thread_id() noexcept;

	/**
	 * Get the number of available logical processors on the machine.
	 *
	 * @return The number of available processors
	 */
	CAKE_API_EXPORT unsigned get_available_processor_count() noexcept;

	/**
	 * Get the processor affinity mask for the current process.
	 *
	 * @return The affinity mask of the currently running process
	 */
	CAKE_API_EXPORT uint64_t get_process_affinity_mask() noexcept;

	/**
	 * Put the current thread to sleep.
	 *
	 * @param duration_ms The duration of the sleep in milliseconds
	 */
	CAKE_API_EXPORT void sleep(size_type duration_ms) noexcept;

	/** Thread state. */
	enum class thread_state {
		/** The thread **object** (not necessarily the underlying thread) has been created but has
		 * not been run.
		 */
		created,

		/** The thread is currently executing. */
		running,

		/** The thread has been terminated. */
		terminated,

		/** The thread object is in an invalid state. */
		invalid
	};

	/**
	 * A platform-agnostic thread class.
	 */
	class CAKE_API_EXPORT thread {
		volatile std::atomic<thread_id> m_id;		/**< The thread ID. */
		volatile thread_handler_ptr m_handler;		/**< Pointer to the thread starting function */
		volatile std::atomic<thread_state> m_state; /**< State of the thread object */
		volatile void *m_param;						/**< Current param */
		volatile HANDLE m_win_handle;				/**< Windows handle to the thread */
		const char *m_name;							/**< The current thread name */

	  public:
		explicit thread(const char *const thread_name = UNNAMED_THREAD_NAME) noexcept:
			m_id { INVALID_THREAD_ID },
			m_handler { nullptr },
			m_state { thread_state::invalid },
			m_param { nullptr },
			m_win_handle { NULL },
			m_name { thread_name }
		{
		}

		thread(const thread &other) = delete;

		thread(thread &&other) noexcept:
			m_id { other.m_id.load(std::memory_order_acquire) },
			m_handler { std::move(other.m_handler) },
			m_state { other.m_state.load(std::memory_order_acquire) },
			m_param { std::move(other.m_param) },
			m_win_handle { std::move(other.m_win_handle) },
			m_name { std::move(other.m_name) }
		{
			other.m_id.store(INVALID_THREAD_ID, std::memory_order_release);
			other.m_state.store(thread_state::invalid, std::memory_order_release);
		}

		~thread() noexcept;

		/**
		 * Initialize a new instance of this class.
		 *
		 * @param handler The thread handler
		 */
		explicit thread(
			thread_handler_ptr const handler,
			const char *const thread_name = UNNAMED_THREAD_NAME) noexcept:
			m_id { INVALID_THREAD_ID },
			m_handler { handler },
			m_state { thread_state::created },
			m_param { nullptr },
			m_win_handle { NULL },
			m_name { thread_name }
		{
		}

		/** Get the implementation-dependent thread ID. */
		thread_id get_id() const noexcept { return m_id.load(std::memory_order_acquire); }

		/** Get the thread handler. */
		thread_handler_ptr get_handler() const noexcept { return m_handler; }

		/** Get the thread state. */
		thread_state get_state() const noexcept { return m_state.load(std::memory_order_acquire); }

		thread &operator=(const thread &other) = delete;

		thread &operator=(thread &&other) noexcept
		{
			m_id.store(other.m_id.load(std::memory_order_acquire), std::memory_order_release);
			m_handler = std::move(other.m_handler);
			m_state.store(other.m_state.load(std::memory_order_acquire), std::memory_order_release);
			m_param = std::move(other.m_param);
			m_win_handle = std::move(other.m_win_handle);
			m_name = std::move(other.m_name);

			other.m_state.store(thread_state::invalid, std::memory_order_release);

			return *this;
		}

		/**
		 * Start the thread.
		 *
		 * @param param The optional parameter to pass to the newly created thread's function.
		 */
		void start(void *param) noexcept;

		/** Wait for the thread to terminate. */
		void join() noexcept;

		/** Wait for the thread to terminate.
		 *
		 * @param timeout_ms The timeout (in milliseconds) to wait before giving up.
		 *
		 * @return True if the thread has been joined, false if the function has given up.
		 */
		bool try_join(unsigned const timeout_ms) noexcept;

		/**
		 * Set the affinity mask for the thread.
		 *
		 * @param mask An affinity mask where a 0 bit indicates the thread should not run on the
		 * given processor and 1 indicates it should.
		 */
		void set_affinity(uint64_t mask) const noexcept;

		/**
		 * Get the name of the thread.
		 *
		 * @return A string containing the name of the thread or `UNNAMED_THREAD_NAME` if the thread
		 * has not been given a name.
		 */
		const char *get_name() const noexcept { return m_name; }

		/**
		 * Thread start service function.
		 *
		 * @param t The thread to run
		 *
		 * @remarks This is an internal method and is not intended to be used outside of the
		 * threading library.
		 */
		static void start_new_thread(thread &t) noexcept;
	};
}

#endif // CAKE_CONCURRENT_THREAD_H
