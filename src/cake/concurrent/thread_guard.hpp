#ifndef CAKE_CONCURRENT_THREAD_GUARD_H
#define CAKE_CONCURRENT_THREAD_GUARD_H

#include "thread.hpp"

namespace cake::concurrent {
	/**
	 * Thread guard to prevent a thread object from being destructed without being joined first.
	 */
	template<typename T>
	class thread_guard {
	  public:
		using thread_type = T;
		using thread_reference = T &;

	  private:
		T &m_t; /**< The guarded thread */

	  public:
		thread_guard(thread_reference t) noexcept: m_t { t } {}
		~thread_guard() noexcept { m_t.join(); }
	};
}

#endif // CAKE_CONCURRENT_THREAD_GUARD_H
