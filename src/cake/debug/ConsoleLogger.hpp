#ifndef CAKE_DEBUG_CONSOLE_LOGGER_H
#define CAKE_DEBUG_CONSOLE_LOGGER_H

#include <cstdio>
#include <cake/api.hpp>
#include "Logger.hpp"

namespace cake::debug {
    /**
     * Console logger.
     */
    class CAKE_API_EXPORT ConsoleLogger : public ILogger {
        protected:
        virtual void log_internal(LogLevel level, const char * const message) noexcept {
			::fprintf(stderr, "%s: %s", str_log_level(level), message);
		}

        public:
		  virtual void flush() noexcept { ::fflush(stderr); }
	};
}

#endif // CAKE_DEBUG_CONSOLE_LOGGER_H
