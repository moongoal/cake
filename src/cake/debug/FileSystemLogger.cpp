#include <cstdlib>
#include "FileSystemLogger.hpp"

namespace cake::debug {
    FileSystemLogger::FileSystemLogger(const char * const path) noexcept {
        errno_t res = fopen_s(&m_file, path, "w");

        if(res != 0) ::abort();
    }

    FileSystemLogger::FileSystemLogger(FileSystemLogger&& other) noexcept {
        m_file = other.m_file;
        other.m_file = nullptr;
    }

    FileSystemLogger::~FileSystemLogger() noexcept {
        if(m_file) fclose(m_file);
    }

    void FileSystemLogger::log_internal(LogLevel level, const char * const message) noexcept {
        fprintf(m_file, "[%s]: %s\n", str_log_level(level), message);
    }

    void FileSystemLogger::flush() noexcept {
        fflush(m_file);
    }
}
