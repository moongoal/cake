#ifndef _CAKE_DEBUG_FILE_SYSTEM_LOGGER_H
#define _CAKE_DEBUG_FILE_SYSTEM_LOGGER_H

#include <cstdio>
#include <cake/api.hpp>
#include "Logger.hpp"

namespace cake::debug {
    class CAKE_API_EXPORT FileSystemLogger final : public ILogger {
        FILE *m_file;

        protected:
        virtual void log_internal(LogLevel level, const char * const message) noexcept;

        public:
        FileSystemLogger(const char * const path) noexcept;
        FileSystemLogger(const FileSystemLogger& other) = delete;
        FileSystemLogger(FileSystemLogger&& other) noexcept;
        virtual ~FileSystemLogger() noexcept;
        
        virtual void flush() noexcept;
    };
}

#endif // _CAKE_DEBUG_FILE_SYSTEM_LOGGER_H
