#include "debug.hpp"

namespace cake::debug {
    ILogger *ILogger::g_logger = new ConsoleLogger();

    ILogger::~ILogger() noexcept {
        if(g_logger == this) {
            g_logger = nullptr;
        }
    }

    void ILogger::set_global() noexcept {
        g_logger = this;
    }
}
