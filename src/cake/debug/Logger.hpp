#ifndef _CAKE_DEBUG_LOGGER_H
#define _CAKE_DEBUG_LOGGER_H

#include <ctime>
#include <cstring>
#include <cake/api.hpp>
#include <cake/algorithms/algorithms.hpp>
#include <cake/algorithms/strings.hpp>

#define LOG_BUFF_LEN 1024

namespace cake::debug {
    enum class LogLevel {
        Debug,
        Info,
        Warning,
        Error
    };

    constexpr const char *str_log_level(LogLevel level) {
        const char *s = nullptr;

        switch(level) {
            #define ENTRY(name) case LogLevel::name: { s = #name; break; }
            ENTRY(Debug)
            ENTRY(Info)
            ENTRY(Warning)
            ENTRY(Error)
            #undef ENTRY
        }

        return s;
    }

    /**
     * Base logger interface.
     */
    class CAKE_API_EXPORT ILogger {
        static ILogger *g_logger;

        protected:
        /**
         * Log a message.
         * 
         * @param level The log level of the message
         * @param message The message to be logged
         */
        virtual void log_internal(LogLevel const level, const char * const message) noexcept = 0;

        public:
        virtual ~ILogger() noexcept;

        template<typename ...Args>
        void log(LogLevel const level, const char * const file, unsigned int const line, Args ...args) noexcept {
			size_type file_len = ::strlen(file);
			const char *const file_end = file + file_len;
			const char *last_path_delim = algorithms::find_last(file, file_end, '/');
			std::time_t time = std::time(nullptr);
            std::tm *time_struct = std::localtime(&time);
			const char log_buff[LOG_BUFF_LEN];

			if (last_path_delim == nullptr)
				last_path_delim = algorithms::find_last(file, file_end, '\\');

            algorithms::value_to_str(log_buff, LOG_BUFF_LEN, "[", std::put_time(time_struct, "%T "), last_path_delim + 1, "@", line, "] ", args...);
            log_internal(level, log_buff);

            #ifdef CAKE_LOG_FLUSH_ALWAYS
            flush();
            #endif // CAKE_LOG_FLUSH_ALWAYS
        }

        /**
         * Flush the logger to the archiving medium.
         */
        virtual void flush() noexcept = 0;

        /**
         * Get the global logger. Will return `nullptr` if no global logger
         * has been defined.
         */
        static ILogger* global() noexcept { return g_logger; }

        /**
         * Set the logger as the global logger.
         */
        void set_global() noexcept;
    };

    /**
     * A dummy logger that does nothing.
     */
    class CAKE_API_EXPORT NullLogger final : public ILogger {
        protected:
        virtual void log_internal(LogLevel level, const char * const message) noexcept {}

        public:
        virtual void flush() noexcept {}
    };
}

#endif // _CAKE_DEBUG_LOGGER_H
