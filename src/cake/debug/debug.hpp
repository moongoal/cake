#ifndef CAKE_DEBUG_H
#define CAKE_DEBUG_H

#include <cstdlib>
#include "Logger.hpp"
#include "FileSystemLogger.hpp"
#include "ConsoleLogger.hpp"

/*
	CAKE_DEBUG_ENABLE_INFO: Add extra debugging information to data structures
*/

#ifdef CAKE_DEBUG_ENABLE_LOGGING

	// Ensure a log level is always present
	#ifndef CAKE_LOG_LEVEL
		#define CAKE_LOG_LEVEL cake::debug::LogLevel::Warning
	#endif // CAKE_LOG_LEVEL

	/**
	 * Log any message
	 */
	#define cake_log_raw(level, ...)                                           \
		do {                                                                   \
			cake::debug::ILogger *logger = cake::debug::ILogger::global();     \
			if (logger) logger->log((level), __FILE__, __LINE__, __VA_ARGS__); \
		} while (false)

#else // CAKE_DEBUG_ENABLE_LOGGING

	#define cake_log_raw(level, ...) ((void)0)

#endif // CAKE_DEBUG_ENABLE_LOGGING

/**
 * Log message with default log level.
 */
#define cake_log(...) cake_log_raw(CAKE_LOG_LEVEL, __VA_ARGS__)

/**
 * Log debug message.
 */
#define cake_log_debug(...) cake_log_raw(cake::debug::LogLevel::Debug, __VA_ARGS__)

/**
 * Log info message.
 */
#define cake_log_info(...) cake_log_raw(cake::debug::LogLevel::Info, __VA_ARGS__)

/**
 * Log warning message.
 */
#define cake_log_warning(...) cake_log_raw(cake::debug::LogLevel::Warning, __VA_ARGS__)

/**
 * Log error message.
 */
#define cake_log_error(...) cake_log_raw(cake::debug::LogLevel::Error, __VA_ARGS__)

/*
 * Memory debugging facility.
 */
#ifdef CAKE_DEBUG_MEMORY
	#define cake_log_memory(...) cake_log_debug(__VA_ARGS__)
#else // CAKE_DEBUG_MEMORY
	#define cake_log_memory(...) ((void)0)
#endif // CAKE_DEBUG_MEMORY

/* Assertions */
#ifdef CAKE_DEBUG_BREAK
	#define CAKE_ASSERT_ACTION __builtin_trap
#else // CAK_DEBUG_BREAK
	#define CAKE_ASSERT_ACTION ::abort
#endif

#ifdef CAKE_DEBUG_ASSERT
	#define cake_assert(expr)                                \
		do {                                                 \
			if (!(expr)) {                                   \
				cake_log_error("Failed assertions ", #expr); \
				CAKE_ASSERT_ACTION();                        \
			}                                                \
		} while (false)
#else // CAKE_DEBUG_ASSERT
	#define cake_assert(expr) ((void)0)
#endif // CAKE_DEBUG_ASSERT

#endif // CAKE_DEBUG_H
