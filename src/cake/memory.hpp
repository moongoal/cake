#ifndef CAKE_MEMORY_H
#define CAKE_MEMORY_H

#include <cake/memory/host/subsystem.hpp>

/* Allocators */
#include <cake/memory/host/arena_allocator.hpp>
#include <cake/memory/host/stack_allocator.hpp>
#include <cake/memory/host/allocator.hpp>
#include <cake/memory/host/allocator_traits.hpp>

/* Utilities */
#include <cake/memory/align.hpp>
#include <cake/memory/allocation_guard.hpp>
#include <cake/memory/allocation_flags.hpp>

#endif // CAKE_MEMORY_H
