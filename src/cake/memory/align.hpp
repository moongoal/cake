#ifndef CAKE_MEMORY_ALIGN_H
#define CAKE_MEMORY_ALIGN_H

#include <type_traits>
#include <cake/meta.hpp>
#include <cake/debug/debug.hpp>

namespace cake::memory {
	/**
	 * Align a value - generally a memory address or a size - to some boundary.
	 * 
	 * @tparam T The aligned type
	 * @tparam A The alignment type
	 * 
	 * @param value_orig The value to align
	 * @param alignment The alignment boundary to use
	 * 
	 * @return The aligned version of value_orig
	 */
	template<typename T, typename A>
	T align(T value_orig, A alignment = alignof(T)) noexcept
	{
		static_assert((std::is_unsigned<T>::value && std::is_integral<T>::value) || std::is_pointer<T>::value);
		static_assert(std::is_unsigned<A>::value && std::is_integral<A>::value);
		cake_assert(is_power_2(alignment));

		const uint64_t mask = (uint64_t)(alignment) - 1;
		const uint64_t u64_addr = (uint64_t)(value_orig);

		return (T)(u64_addr + mask & ~mask);
	}
}

#endif // CAKE_MEMORY_ALIGN_H
