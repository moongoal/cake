#ifndef CAKE_MEMORY_ALLOCATIONFLAGS_H
#define CAKE_MEMORY_ALLOCATIONFLAGS_H

#include <cstdint>

namespace cake::memory {
    typedef uint32_t allocation_flags;

    allocation_flags constexpr CAKE_ALLOC_DEFAULT = 0; /**< Default allocation mode */

     /**
      * Pin memory after allocation.
      * 
      * Pinned memory cannot be moved or resized.
      * 
     */
    allocation_flags constexpr CAKE_ALLOC_PINNED = 1;
}

#endif // CAKE_MEMORY_ALLOCATIONFLAGS_H
