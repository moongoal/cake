#ifndef CAKE_MEMORY_ALLOCATION_GUARD_H
#define CAKE_MEMORY_ALLOCATION_GUARD_H

namespace cake::memory {
	template<typename T, typename Allocator>
	class allocation_guard final {
		Allocator &m_allocator;
		T *m_ptr;

	  public:
		allocation_guard(Allocator &allocator, T *ptr) noexcept: m_allocator(allocator), m_ptr(ptr)
		{
		}

		allocation_guard(allocation_guard &&other) noexcept:
			m_allocator(other.m_allocator),
			m_ptr(other.m_ptr)
		{
			guard.m_ptr = nullptr;
		}

		allocation_guard(const allocation_guard &other) = delete;
		~allocation_guard() { m_allocator.free(m_ptr); }

		[[nodiscard]] T *value() noexcept { return m_ptr; }
		void invalidate() noexcept { m_ptr = nullptr; }
		void replace(T *new_ptr) noexcept { m_ptr = new_ptr; }
	};
}

#endif // CAKE_MEMORY_ALLOCATION_GUARD_H
