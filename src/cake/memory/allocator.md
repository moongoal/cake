# Allocator

## Contract
* `void *allocate(AllocSize const size, AlignSize const alignment, AllocationFlags const flags) noexcept`
* `template<typename T> T allocate(size_type const n = 1) noexcept`
* `void free(void * const ptr) noexcept`
* `void *reallocate(void * const ptr, AllocSize const new_size) noexcept`
* `void clear() noexcept`

### `allocate()`
Returns an aligned pointer to a memory location of at least `size` size. The typed version is just a shortcut to `reinterpret_cast<T *>(allocate(sizeof(T), alignof(T), flags))` - no constructor is invoked.

### `free()`
Invalidates the input pointer and returns the memory it represents to the allocator.

### `reallocate()`
Grows, shrinks and eventually relocates the memory referenced by `ptr` in order to resize it to `new_size`.

### `clear()`
Immediately clear the whole allocator, returning it to its initial state. Any pointer allocated by the allocator is invalidated.

### Type definitions
All allocators must define an `AllocSize` type member that defines the size type for any allocations and an `AlignSize` type member defining the the size type of any alignment boundaries.

### Shared behaviour
All the functions that accept a pointer do nothing if the pointer is `nullptr`.
Every free()/reallocate() must ensure the pointer has been allocated within the given allocator at least by debug assertion.

### Allocation flags
Allocations can be flagged to behave in some special way. Currently only one flag is supported:
- `CAKE_ALLOC_PINNED`: Pins memory so that it cannot be moved or resized.
