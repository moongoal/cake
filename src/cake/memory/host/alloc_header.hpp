#ifndef CAKE_MEMORY_ALLOC_H
#define CAKE_MEMORY_ALLOC_H

#include <cake/memory/align.hpp>
#include <cake/memory/allocation_flags.hpp>
#include <cake/debug/debug.hpp>
#include "config.hpp"

namespace cake::memory::host {
	/**
	 * Allocation medatata.
	 */
	struct alloc_header {
		size_type m_alloc_sz;						  /**< Allocation size in bytes */
		size_type m_alignment;						  /**< Allocation alignment */
		cake::memory::allocation_flags const m_flags; /**< Allocation flags */

		alloc_header(
			size_type const alloc_sz,
			size_type const alignment,
			cake::memory::allocation_flags const flags) noexcept:
			m_alloc_sz { alloc_sz },
			m_alignment { alignment },
			m_flags { flags }
		{
		}

		bool is_pinned() const noexcept { return m_flags & CAKE_ALLOC_PINNED; }

		/**
		 * Return the allocation header for a given pointer, assuming it's pointing at the start of
		 * the allocation.
		 *
		 * @param ptr The pointer to the beginning of the allocation
		 *
		 * @return The allocation header for the allocation
		 */
		template<size_t PageSize>
		static alloc_header *get_alloc_header_for_ptr(void *const ptr) noexcept
		{
			return reinterpret_cast<alloc_header *>(
				align(reinterpret_cast<uint8_t *>(ptr), PageSize) - PageSize);
		}
	};
}

#endif // CAKE_MEMORY_ALLOC_H
