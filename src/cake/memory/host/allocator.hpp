#ifndef CAKE_MEMORY_HOST_ALLOCATOR_H
#define CAKE_MEMORY_HOST_ALLOCATOR_H

#include <limits>
#include <type_traits>
#include <memory>
#include <cake/api.hpp>
#include "arena_allocator.hpp"
#include "cake/memory/host/subsystem.hpp"
#include "cake/types.hpp"

namespace cake::memory::host {
	// TODO: Tweak this value
	constexpr size_type large_small_threshold = 1024 * 1024; // 1 MiB

	template<typename T>
	class allocator {
	  public:
		using pointer = T *;
		using size_type = cake::size_type;

	  private:
		static size_type compute_object_total_size(size_type const n) noexcept
		{
			return sizeof(T) * n;
		}

	  public:
		constexpr allocator() noexcept {}

		constexpr allocator(const allocator &other) noexcept {}

		template<typename U>
		constexpr allocator(const allocator<U> &other) noexcept
		{
		}

		constexpr allocator(allocator &&other) noexcept {}

		template<typename U>
		constexpr allocator(allocator<U> &&other) noexcept
		{
		}

		[[nodiscard]] constexpr pointer allocate(size_type n) noexcept
		{
			if (compute_object_total_size(n) < large_small_threshold) {
				return reinterpret_cast<pointer>(
					get_small_heap_allocator().allocate(sizeof(T) * n, alignof(T)));
			} else {
				return reinterpret_cast<pointer>(
					get_large_heap_allocator().allocate(sizeof(T) * n, alignof(T)));
			}
		}

		[[nodiscard]] constexpr pointer reallocate(pointer p, size_type n, size_type new_n) noexcept
		{
			if (compute_object_total_size(n) < large_small_threshold) {
				return reinterpret_cast<pointer>(
					get_small_heap_allocator().reallocate(p, sizeof(T) * new_n));
			} else {
				return reinterpret_cast<pointer>(
					get_large_heap_allocator().reallocate(p, sizeof(T) * new_n));
			}
		}

		constexpr void deallocate(pointer p, size_type n) noexcept
		{
			if (compute_object_total_size(n) < large_small_threshold) {
				get_small_heap_allocator().free(p);
			} else {
				get_large_heap_allocator().free(p);
			}
		}

		constexpr allocator &operator=(allocator &other) noexcept { return *this; }
	};

	template<typename T>
	constexpr bool operator==(allocator<T> &a, allocator<T> &b) noexcept
	{
		return true;
	}

	template<typename T>
	constexpr bool operator!=(allocator<T> &a, allocator<T> &b) noexcept
	{
		return !(a == b);
	}

	template<typename T, typename U>
	constexpr bool operator==(allocator<T> &a, allocator<U> &b) noexcept
	{
		return a == std::allocator_traits<allocator<U>>::template rebind<T>::other(b);
	}

	template<typename T, typename U>
	constexpr bool operator!=(allocator<T> &a, allocator<U> &b) noexcept
	{
		return !(a == b);
	}
}

#endif // CAKE_MEMORY_HOST_ALLOCATOR_H
