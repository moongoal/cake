#ifndef CAKE_MEMORY_HOST_ALLOCATOR_TRAITS_H
#define CAKE_MEMORY_HOST_ALLOCATOR_TRAITS_H

#include <memory>

namespace cake::memory::host {
	template<typename Alloc>
	struct allocator_traits: public std::allocator_traits<Alloc> {
		static constexpr typename std::allocator_traits<Alloc>::pointer reallocate(
			Alloc &alloc,
			typename std::allocator_traits<Alloc>::pointer const p,
			typename std::allocator_traits<Alloc>::size_type const n,
			typename std::allocator_traits<Alloc>::size_type const new_n) noexcept
		{
			return alloc.reallocate(p, n, new_n);
		}
	};
}

#endif // CAKE_MEMORY_HOST_ALLOCATOR_TRAITS_H
