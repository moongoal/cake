#ifndef CAKE_MEMORY_HOST_ARENA_ALLOCATOR_H
#define CAKE_MEMORY_HOST_ARENA_ALLOCATOR_H

#include <cstdint>
#include <type_traits>
#include <cstdlib>
#include <cake/api.hpp>
#include <cake/types.hpp>
#include <cake/memory/align.hpp>
#include <cake/memory/allocation_flags.hpp>
#include "config.hpp"
#include "free_list_realloc_helper.hpp"
#include "alloc_header.hpp"
#include "free_list.hpp"

#ifdef cake_assert_ptr_ownership
	#error cake_assert_ptr_ownership macro was already defined.
#else // cake_assert_ptr_ownership
	#define cake_assert_ptr_ownership(ptr)                        \
		cake_assert(                                              \
			reinterpret_cast<uint8_t *>(ptr) >= m_memory.m_origin \
			&& reinterpret_cast<uint8_t *>(ptr) < m_memory.m_origin + m_memory.m_size)
#endif // cake_assert_ptr_ownership

namespace cake::memory::host {
	/**
	 * Arena allocator.
	 *
	 * This allocator allocates memory on a per-page basis.
	 *
	 * @tparam PageSize The memory page size in bytes
	 *
	 * @see cake::memory::host::defrag_arena_allocator
	 */
	template<size_type PageSize>
	class arena_allocator {
	  public:
		using size_type = size_type;
		using allocation_flags = cake::memory::allocation_flags;

		static constexpr size_type page_size = PageSize;

	  protected:
		free_list m_free;				 /**< List of free chunks */
		memory_region<uint8_t> m_memory; /**< Memory region */

	  public:
		arena_allocator() noexcept: m_memory {} {}

		arena_allocator(const generic_memory_region &memory) noexcept:
			m_free {},
			m_memory { memory }
		{
			clear();
		}

		arena_allocator(arena_allocator &&other) noexcept = default;

		arena_allocator &operator=(arena_allocator &&other) noexcept
		{
			m_free = std::move(other.m_free);
			m_memory = std::move(other.m_memory);

			return *this;
		}

		/**
		 * Allocate space for one or more objects.
		 *
		 * @tparam T The object type to allocate
		 *
		 * @param size The number of bytes to allocate
		 * @param alignment The allocation alignment
		 * @param flags The allocation flags
		 *
		 * @return A pointer to a continuous chunk of memory large enough to contain *n* objects of
		 * type *T*
		 *
		 * @note This member function only allocates space, no constructor is called.
		 */
		void *allocate(
			size_type const size,
			size_type const alignment,
			allocation_flags const flags = CAKE_ALLOC_DEFAULT) noexcept
		{
			size_type const aligned_offset = align(
				sizeof(alloc_header),
				alignment); // This is also the overall extra space (metadata + padding)
			size_type const aligned_size = align(size + aligned_offset, page_size);
			free_list::find_node_result result;
			bool node_found = m_free.find_node_by_size(aligned_size, &result);

			cake_assert(node_found);

			alloc_header *const hdr_ptr = reinterpret_cast<alloc_header *>(
				m_free.reduce_size_with_previous(&result.m_node, result.m_prev_node, aligned_size));

			// Set allocation header
			new (hdr_ptr) alloc_header(aligned_size, alignment, flags);
			return reinterpret_cast<uint8_t *>(hdr_ptr) + aligned_offset;
		}

		void free(void *const ptr) noexcept
		{
			if (!ptr) { return; }

			cake_assert_ptr_ownership(ptr);
			alloc_header *const hdr_ptr = alloc_header::get_alloc_header_for_ptr<page_size>(ptr);
			m_free.add_node(free_list::memory_region_type { hdr_ptr, hdr_ptr->m_alloc_sz });
		}

		void *reallocate(void *const ptr, size_type const new_size) noexcept
		{
			alloc_header *const hdr_ptr = alloc_header::get_alloc_header_for_ptr<page_size>(ptr);

			cake_assert_ptr_ownership(ptr);
			cake_assert(!hdr_ptr->is_pinned()); // Pinned memory cannot be reallocated

			// Nothing to change
			if (!ptr || hdr_ptr->m_alloc_sz == new_size) { return ptr; }

			void *new_ptr; // WORK FROM HERE

			if (new_size < hdr_ptr->m_alloc_sz) { // Shrink-reallocate
				new_ptr = shrink_realloc<arena_allocator>(m_free, ptr, new_size);
			} else {
				new_ptr = grow_realloc<arena_allocator>(*this, m_free, ptr, new_size);
			}
			// Grow-reallocate

			return new_ptr;
		}

		void clear() noexcept
		{
			m_free.clear();
			m_free.add_node(
				generic_memory_region { align(m_memory.m_origin, page_size), m_memory.m_size });
		}

		uint8_t *get_base() const noexcept { return m_memory.m_origin; }
		size_type get_memory_size() const noexcept { return m_memory.m_size; }

		[[nodiscard]] size_type get_free_memory() noexcept { return m_free.get_size(); }
	};
}

#undef cake_assert_ptr_ownership

#endif // CAKE_MEMORY_HOST_ARENA_ALLOCATOR_H
