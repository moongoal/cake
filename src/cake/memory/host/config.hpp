#ifndef CAKE_MEMORY_CONFIG_H
#define CAKE_MEMORY_CONFIG_H

#include <type_traits>
#include <cstdint>
#include <cake/meta.hpp>
#include <cake/types.hpp>

namespace cake::memory::host {
	constexpr size_type PAGE_SIZE_LARGE = 64 * 1024; /*< Default large page size in bytes */
	constexpr size_type PAGE_SIZE_SMALL = 4 * 1024;	 /*< Default small page size in bytes */

	// Sanity checks
	static_assert(cake::is_power_2(PAGE_SIZE_LARGE), "Page size must be a power of 2");
	static_assert(cake::is_power_2(PAGE_SIZE_SMALL), "Page size must be a power of 2");
}

#endif // CAKE_MEMORY_CONFIG_H
