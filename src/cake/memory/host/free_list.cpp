#include <cstdlib>
#include "cake/utils.hpp"
#include "free_list.hpp"

namespace cake::memory::host {
	free_list::node_pointer free_list::add_node(const memory_region_type &memory) noexcept
	{
		auto [ptr, size] = memory;
		node_pointer node_ptr;

		// Node has to be prepended to the list
		if (ptr < m_first_ptr->get_ptr() || !m_first_ptr) {
			m_first_ptr = node_ptr = new (ptr) free_list_node { size, m_first_ptr };
		} else { // Node has to be inserted or appended to the list
			node_pointer it = m_first_ptr;
			node_pointer prev;

			while (it) {
				if (it->get_ptr() < ptr && it->has_next() && it->get_next()->get_ptr() > ptr) {
					break;
				}

				prev = it;
				it = it->get_next();
			}

			if (it) { // Add the node between it and its next sibling
				node_ptr = new (ptr) free_list_node { size, it->get_next() };

				it->set_next(node_ptr);
			} else { // Append to the end of the list
				node_ptr = new (ptr) free_list_node { size, nullptr };

				prev->set_next(node_ptr);
			}

			// Coalesce node with previous if adjacent
			if (prev->is_left_adjacent(*node_ptr)) {
				coalesce(*prev, *node_ptr);
				node_ptr = prev;
			}
		}

		// Coalesce node with next if adjacent
		if (node_ptr->has_next() && node_ptr->is_left_adjacent(*node_ptr->get_next())) {
			coalesce(*node_ptr, *node_ptr->get_next());
		}

		return node_ptr;
	}

	void free_list::remove_node_with_previous(node_reference node, node_pointer const prev) noexcept
	{
		node_pointer const next = node.get_next();

		if (prev) {
			prev->set_next(next);
		} else {
			m_first_ptr = next;
		}
	}

	free_list::node_pointer free_list::find_previous_node(node_const_reference node) noexcept
	{
		if (node.get_ptr() == m_first_ptr) { return nullptr; }

		node_pointer it, prev;

		for (it = m_first_ptr; it->get_ptr() != node.get_ptr(); prev = it, it = it->get_next())
			;

		return prev;
	}

	void free_list::coalesce(node_reference node1, node_reference node2) noexcept
	{
		cake_assert(node1.is_left_adjacent(node2));

		node1.set_size(node1.get_size() + node2.get_size());
		remove_node_with_previous(node2, &node1);
	}

	bool
	free_list::find_node_by_size(size_type const min_size, find_node_result *const result) noexcept
	{
#define it result->m_node
#define prev result->m_prev_node
		for (prev = nullptr, it = m_first_ptr; it != nullptr; prev = it, it = it->get_next()) {
			if (it->get_size() >= min_size) { return true; }
		}
#undef it
#undef prev
		return false;
	}

	free_list::size_type free_list::get_size() const noexcept
	{
		size_type sz = 0;

		for (node_pointer it = m_first_ptr; it != nullptr; it = it->get_next()) {
			sz += it->get_size();
		}

		return sz;
	}

	free_list::pointer free_list::reduce_size_with_previous(
		node_pointer *const node,
		node_pointer const prev,
		size_type const size) noexcept
	{
		cake_assert((*node)->get_size() >= size); // Can't have negative size
		cake_assert(
			(*node)->get_size() - size >= sizeof(free_list_node)
			|| (*node)->get_size() - size
				   == 0); // Not enough room for storing the remaining memory node

		pointer const ret_ptr = (*node)->get_ptr();
		pointer const remaining_node_ptr = reinterpret_cast<uint8_t *>(ret_ptr) + size;
		size_type const remaining_size = (*node)->get_size() - size;

		remove_node_with_previous(
			**node, prev); // This node now contains only memory that has not to be managed anymore

		*node = remaining_size ? add_node(memory_region_type { remaining_node_ptr, remaining_size })
							   : nullptr;

		return ret_ptr;
	}
}
