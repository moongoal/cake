#ifndef CAKE_MEMORY_FREE_LIST_H
#define CAKE_MEMORY_FREE_LIST_H

#include <cstdint>
#include <type_traits>
#include <limits>
#include <cake/api.hpp>
#include <cake/types.hpp>
#include <cake/utils.hpp>
#include <cake/debug/debug.hpp>
#include "config.hpp"

namespace cake::memory::host {
	struct free_list_node final {
	  public:
		using node_pointer = free_list_node *;
		using node_reference = free_list_node &;
		using node_const_reference = const free_list_node &;
		using pointer = uint8_t *;

	  private:
		node_pointer m_next;  /**< Pointer to next node */
		size_type m_alloc_sz; /**< Amount of memory represented by this node */

	  public:
		free_list_node() noexcept: m_alloc_sz { 0 }, m_next { nullptr } {}

		free_list_node(size_type const alloc_sz, node_pointer const next_ptr) noexcept:
			m_alloc_sz { alloc_sz },
			m_next { next_ptr }
		{
		}

		~free_list_node() noexcept { make_unused(); }

		/** Make the node unused. */
		void make_unused() noexcept
		{
			m_alloc_sz = 0;
			unset_next();
		}

		/** Tell whether the node is currently in use or not */
		bool is_used() const noexcept { return m_alloc_sz > 0; }

		bool has_next() const noexcept { return m_next != nullptr; }

		void *get_ptr() const noexcept
		{
			return reinterpret_cast<void *>(const_cast<node_pointer>(this));
		}

		size_type get_size() const noexcept { return m_alloc_sz; }

		void set_size(size_type const sz) noexcept { m_alloc_sz = sz; }

		node_pointer get_next() const noexcept { return m_next; }

		void unset_next() noexcept { m_next = nullptr; }

		void set_next(node_pointer const ptr) noexcept { m_next = ptr; }

		free_list_node &operator=(free_list_node &other)
		{
			m_next = other.m_next;
			m_alloc_sz = other.m_alloc_sz;

			return *this;
		}

		/**
		 * Return a boolean value indicating whether `other` is adjacent to the right of this node.
		 *
		 * @param other Node to test for right-adjacency
		 *
		 * @return True if `other` represents a memory location adjacent to the one represented by
		 * this node and its address in memory is above the address of the memory location
		 * represented by this node.
		 */
		bool is_left_adjacent(node_const_reference other) const noexcept
		{
			return reinterpret_cast<uint8_t *>(get_ptr()) + get_size() == get_next()->get_ptr();
		}
	};

	/**
	 * List intended to be used specifically for storing information about
	 * free memory within an allocator.
	 */
	class CAKE_API_EXPORT free_list {
	  public:
		using node_pointer = free_list_node *;
		using node_reference = free_list_node &;
		using node_const_reference = const free_list_node &;
		using pointer = void *;
		using size_type = cake::size_type;
		using memory_region_type = generic_memory_region;

		/**
		 * Find node result.
		 */
		struct find_node_result {
			node_pointer m_node;	  /**< Found node */
			node_pointer m_prev_node; /**< Previous node of `m_node` */
		};

	  private:
		node_pointer m_first_ptr; /**< Pointer to first node */

	  public:
		/**
		 * Initialize a new instance of this class.
		 */
		free_list() noexcept: m_first_ptr { nullptr } {}
		free_list(const free_list &other) = delete;

		free_list(free_list &&other) noexcept: m_first_ptr { other.m_first_ptr }
		{
			other.m_first_ptr = nullptr;
		}

		free_list &operator=(free_list &&other) noexcept
		{
			m_first_ptr = other.m_first_ptr;
			other.m_first_ptr = nullptr;

			return *this;
		}

		/**
		 * Add a free memory region to the list.
		 *
		 * @param memory The memory region associated with the node
		 *
		 * @return The pointer to the node representing the memory region. The returned node is
		 * effectively stored at the address of the free memory region so the returned value will
		 * always be the same as `ptr`.
		 */
		node_pointer add_node(const memory_region_type &memory) noexcept;

		/**
		 * Remove a node from the list.
		 * @remarks This calls `remove_node_with_previous()`.
		 *
		 * @param node The node to remove
		 */
		void remove_node(node_reference node) noexcept
		{
			remove_node_with_previous(node, find_previous_node(node));
		}

		/**
		 * Remove a node from the list.
		 *
		 * @param node The node to remove
		 * @param prev The previous node of `node` or `nullptr`
		 */
		void remove_node_with_previous(node_reference node, node_pointer const prev) noexcept;

		/**
		 * Find a node given its pointer.
		 *
		 * @param ptr The pointer of the node to find
		 *
		 * @return A pointer to the node or `nullptr` if the pointer is not directly contained in
		 * this list
		 */
		node_pointer find_node_by_ptr(pointer const ptr) noexcept
		{
			node_pointer it;

			for (it = m_first_ptr; it != nullptr; it = it->get_next()) {
				if (it->get_ptr() == ptr) { break; }
			}

			return it;
		}

		/**
		 * Find the previous sibling of a given node.
		 *
		 * @param node The node to find the previous of
		 *
		 * @return The previous node of `node` or `nullptr` if `node` has no previous (first of the
		 * list)
		 */
		node_pointer find_previous_node(node_const_reference node) noexcept;

		/**
		 * Find a node that is *at least* `min_size` bytes big.
		 *
		 * @param min_size Required size specification for the returned node
		 * @param result Pointer to result structure that will be filled with a pointer to a node
		 * having a size of at least `min_size` bytes and its previous sibling.
		 *
		 * @return True on success or false if no node was found.
		 */
		bool find_node_by_size(size_type const min_size, find_node_result *const result) noexcept;

		/**
		 * Coalesce two adjacent nodes returning the union node.
		 *
		 * @param node1 The first node
		 * @param noed2 The second node
		 */
		void coalesce(node_reference node1, node_reference node2) noexcept;

		/**
		 * Reduce the size of the node and remove it from the list if its remaining size reaches 0.
		 *
		 * @param node The node to reduce the size of
		 * @param size The number of bytes to subtract from the node's size
		 *
		 * @return A pointer to a memory location of size `size`, detached from `node`
		 */
		pointer reduce_size(node_pointer *const node, size_type const size) noexcept
		{
			return reduce_size_with_previous(node, find_previous_node(**node), size);
		}

		/**
		 * Reduce the size of the node and remove it from the list if its remaining size reaches 0.
		 *
		 * @param node The node to reduce the size of
		 * @param prev The previous node of `node`
		 * @param size The number of bytes to subtract from the node's size
		 *
		 * @return A pointer to a memory location of size `size`, detached from `node`
		 * @remarks `node` is updated with the new node after the reduction
		 */
		pointer reduce_size_with_previous(
			node_pointer *const node,
			node_pointer const prev,
			size_type const size) noexcept;

		/**
		 * Return the first node.
		 *
		 * @return A pointer to the first node or `nullptr` if the list is empty
		 */
		node_pointer get_first_node() noexcept { return m_first_ptr; }

		/**
		 * Clears the list.
		 */
		void clear() noexcept { m_first_ptr = nullptr; }

		/**
		 * Get the free memory size, in bytes.
		 *
		 * @return The total size of the free memory referenced by this list, in bytes
		 */
		size_type get_size() const noexcept;
	};
}

#endif // CAKE_MEMORY_FREE_LIST_H
