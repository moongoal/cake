#ifndef CAKE_MEMORY_FREE_LIST_REALLOC_HELPER_H
#define CAKE_MEMORY_FREE_LIST_REALLOC_HELPER_H

#include <utility>
#include <cstdlib>
#include <cake/types.hpp>
#include <cake/memory/align.hpp>
#include "alloc_header.hpp"
#include "free_list.hpp"
#include "config.hpp"

namespace cake::memory::host {
	template<typename Allocator>
	void *
	shrink_realloc(free_list &m_free, void *const ptr, size_type const requested_size) noexcept
	{
		alloc_header *const hdr_ptr =
			alloc_header::get_alloc_header_for_ptr<Allocator::page_size>(ptr);

		// Don't modify allocation header if within same page
		if (hdr_ptr->m_alloc_sz - requested_size < Allocator::page_size) { return ptr; }

		size_type const requested_size_aligned = align(requested_size, Allocator::page_size);
		size_type const sz_diff = requested_size_aligned - hdr_ptr->m_alloc_sz;
		uint8_t *const alloc_ptr = reinterpret_cast<uint8_t *>(ptr);

		m_free.add_node(
			free_list::memory_region_type { alloc_ptr + requested_size_aligned, sz_diff });
		hdr_ptr->m_alloc_sz = requested_size_aligned;

		return ptr;
	}

	template<typename Allocator>
	void *grow_realloc(
		Allocator &allocator,
		free_list &m_free,
		void *const ptr,
		size_type const requested_size) noexcept
	{
		alloc_header *hdr_ptr = alloc_header::get_alloc_header_for_ptr<Allocator::page_size>(ptr);
		size_type sz_diff = requested_size - hdr_ptr->m_alloc_sz;
		uint8_t *alloc_ptr = reinterpret_cast<uint8_t *>(ptr);
		free_list_node *next_node = m_free.find_node_by_ptr(alloc_ptr + hdr_ptr->m_alloc_sz);

		if (!next_node || next_node->get_size() < sz_diff) {
			// No adjacent memory available or adjacent memory not sufficient
			void *const new_ptr =
				allocator.allocate(requested_size, hdr_ptr->m_alignment, hdr_ptr->m_flags);

			cake_assert(new_ptr); // Memory is available

			return new_ptr;
		}

		// Adjacent memory available and big enough to avoid moving data around
		hdr_ptr->m_alloc_sz = requested_size;

		m_free.reduce_size(&next_node, sz_diff);

		return ptr;
	}
}

#endif // CAKE_MEMORY_FREE_LIST_REALLOC_HELPER_H
