#ifndef CAKE_MEMORY_HOST_STACK_ALLOCATOR_H
#define CAKE_MEMORY_HOST_STACK_ALLOCATOR_H

#include <cstdint>
#include <cake/api.hpp>
#include <cake/types.hpp>
#include <cake/utils.hpp>
#include <cake/debug/debug.hpp>
#include <cake/memory/allocation_flags.hpp>
#include <cake/memory/align.hpp>

/** Asserts the ownership of a given pointer is of the current allocator */
#ifdef cake_assert_ptr_ownership
	#error cake_assert_ptr_ownership macro was already defined.
#endif // cake_assert_ptr_ownership

#define cake_assert_ptr_ownership(ptr)                       \
	cake_assert(                                             \
		reinterpret_cast<uint8_t *>(ptr) > m_memory.m_origin \
		&& reinterpret_cast<uint8_t *>(ptr) < m_memory.m_origin + m_memory.m_size)

namespace cake::memory::host {
	/**
	 * Stack allocator - allocates memory using a stack-shaped structure.
	 *
	 * This allocator is generally fast but only allows reallocation to
	 * happen at the top of the stack (only the last not-yet-freed allocation
	 * will succeed reallocating) if it would need to grow the allocated region. All operations
	 * happen in O(1) and no memory fragmentation is possible.
	 *
	 */
	class stack_allocator {
	  public:
		/** Allocation header */
		struct alloc_header {				// TODO: Check if it's feasible to move this to
											// cake::memory::alloc_header
			size_type m_alloc_sz;			/**< Allocation size */
			size_type const m_padding_sz;	/**< Padding size */
			allocation_flags const m_flags; /**< Allocation flags */

			alloc_header(
				size_type const alloc_sz,
				size_type const padding_sz,
				allocation_flags const flags) noexcept:
				m_alloc_sz(alloc_sz),
				m_padding_sz(padding_sz),
				m_flags(flags)
			{
			}
		};

	  private:
		memory_region<uint8_t> m_memory; /**< Memory region */
		uint8_t *m_head_ptr;			 /**< Pointer to the head of the stack */

	  public:
		stack_allocator(const generic_memory_region &memory) noexcept: m_memory { memory }
		{
			clear();
		}

		void clear() noexcept { m_head_ptr = m_memory.m_origin; }

		void free(void *const ptr) noexcept
		{
			if (!ptr) { return; }

			alloc_header *hdr_ptr = reinterpret_cast<alloc_header *>(ptr) - 1;
			cake_assert_ptr_ownership(ptr);

			m_head_ptr = reinterpret_cast<uint8_t *>(hdr_ptr) - hdr_ptr->m_padding_sz;
		}

		/**
		 * Reallocate a chunk of memory.
		 *
		 * @tparam T The type of object to reallocate
		 *
		 * @param ptr The pointer to the memory chunk to reallocate
		 * @param new_length The new length to assign to the reallocated memory chunk
		 *
		 * @note Reallocation within this allocator is only available if *ptr*
		 * is currently at the top of the stack or the reallocation would result in shrinking the
		 * allocated memory region. Attempts to reallocate other memory will cause an assertion to
		 * fail.
		 */
		void *reallocate(void *const ptr, size_type const new_size) noexcept
		{
			// TODO: Rename this
			uint8_t *alloc_ptr_aligned = reinterpret_cast<uint8_t *>(ptr);
			alloc_header *hdr_ptr = reinterpret_cast<alloc_header *>(ptr) - 1;
			uint8_t *next_head_ptr = alloc_ptr_aligned + hdr_ptr->m_alloc_sz;
			bool is_ptr_top = next_head_ptr == m_head_ptr; // Is ptr at the top of the stack?

			cake_assert_ptr_ownership(ptr);
			cake_assert(
				!(hdr_ptr->m_flags & CAKE_ALLOC_PINNED)); // Pinned memory is not reallocatable

			if (new_size > hdr_ptr->m_alloc_sz) { // Growing allocation
				cake_assert(is_ptr_top);		  // TODO: Allow for growing allocations
			}

			m_head_ptr = utils::choose(alloc_ptr_aligned + new_size, m_head_ptr, is_ptr_top);
			hdr_ptr->m_alloc_sz = utils::choose(new_size, hdr_ptr->m_alloc_sz, is_ptr_top);

			return ptr;
		}

		void *get_base() noexcept { return m_memory.m_origin; }
		void *get_head() noexcept { return m_head_ptr; }
		size_type get_size() noexcept { return m_memory.m_size; }

		/**
		 * Allocate space for one or more objects.
		 *
		 * @tparam T The object type to allocate
		 *
		 * @param n The number of objects to allocate
		 * @param flags The allocation flags
		 *
		 * @return A pointer to a continuous chunk of memory large enough to contain *n* objects of
		 * type *T*
		 *
		 * @note This member function only allocates space, no constructor is called.
		 */
		void *allocate(
			size_type const size,
			size_type const alignment,
			allocation_flags const flags = CAKE_ALLOC_DEFAULT) noexcept
		{
			cake_assert(size > 0);
			cake_assert(alignment > 0);

			uint8_t *const alloc_ptr = m_head_ptr + sizeof(alloc_header);
			uint8_t *const alloc_ptr_aligned = align(alloc_ptr, alignment);
			alloc_header *const hdr_ptr = reinterpret_cast<alloc_header *>(alloc_ptr_aligned) - 1;
			size_type const padding = static_cast<size_type>(alloc_ptr_aligned - alloc_ptr);

			new (hdr_ptr) alloc_header(size, padding, flags);
			m_head_ptr = alloc_ptr_aligned + size;

			return alloc_ptr_aligned;
		}
	};
}

#undef cake_assert_ptr_ownership

#endif // CAKE_MEMORY_HOST_STACK_ALLOCATOR_H
