#ifdef _WIN32
	#include <Windows.h>
#else
	#error Unsupported OS
#endif // _WIN32

#include <cake/types.hpp>
#include <cake/debug/debug.hpp>
#include <cake/memory/host/config.hpp>
#include <cake/memory/align.hpp>
#include "subsystem.hpp"

using namespace cake;
using namespace cake::memory;
using namespace cake::memory::host;

namespace {
	small_heap_type small_heap;
	large_heap_type large_heap;

	void *alloc(size_type const mem_sz) noexcept
	{
#ifdef _WIN32
		void *const mem_ptr =
			::VirtualAlloc(NULL, mem_sz, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#endif // _WIN32
		cake_assert(mem_ptr);

		return mem_ptr;
	}
}

namespace cake::memory::host {
	void startup(const configuration &config) noexcept
	{
		void *const sh_mem_ptr = ::alloc(config.mem_size_small);
		void *const lh_mem_ptr = ::alloc(config.mem_size_large);

		::small_heap =
			small_heap_type { generic_memory_region { sh_mem_ptr, config.mem_size_small } };
		::large_heap =
			large_heap_type { generic_memory_region { lh_mem_ptr, config.mem_size_large } };
	}

	void shutdown() noexcept
	{
#ifdef _WIN32
		::VirtualFree(::small_heap.get_base(), 0, MEM_RELEASE);
		::VirtualFree(::large_heap.get_base(), 0, MEM_RELEASE);
#endif // _WIN32
	}

	small_heap_type &get_small_heap_allocator() noexcept { return ::small_heap; }

	large_heap_type &get_large_heap_allocator() noexcept { return ::large_heap; }
}
