#ifndef CAKE_MEMORY_HOST_SUBSYSTEM_H
#define CAKE_MEMORY_HOST_SUBSYSTEM_H

#include <cake/types.hpp>
#include <cake/api.hpp>
#include <cake/debug/debug.hpp>
#include <cake/memory/host/config.hpp>
#include <cake/memory/host/arena_allocator.hpp>

namespace cake::memory::host {
	using small_heap_type = arena_allocator<PAGE_SIZE_SMALL>;
	using large_heap_type = arena_allocator<PAGE_SIZE_LARGE>;
	
	struct configuration {
		size_type mem_size_small; /**< Small-heap memory size, in bytes */
        size_type mem_size_large; /**< Large-heap memory size, in bytes */
	};

	CAKE_API_EXPORT void startup(const configuration &config) noexcept;
	CAKE_API_EXPORT void shutdown() noexcept;

	CAKE_API_EXPORT small_heap_type& get_small_heap_allocator() noexcept;
	CAKE_API_EXPORT large_heap_type& get_large_heap_allocator() noexcept;
}

#endif // CAKE_MEMORY_HOST_SUBSYSTEM_H
