#ifndef CAKE_META_H
#define CAKE_META_H

#include <limits>
#include "types.hpp"

namespace cake {
	/**
	 * Check if value is a power of two.
	 *
	 * @param n The value to check
	 *
	 * @return A boolean indicating whether the input argument is a power of two.
	 */
	constexpr bool is_power_2(size_type n) { return (n > 0) && ((n & (n - 1)) == 0); }

	/**
	 * Compute the length of a fixed-size array.
	 *
	 * @tparam T Array type
	 * @tparam N Array length
	 *
	 * @return The number of elements within the array
	 */
	template<typename T, size_type N>
	constexpr size_type array_length(const T (&)[N])
	{
		return N;
	}
}

#endif // CAKE_META_H
