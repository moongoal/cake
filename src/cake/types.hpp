#ifndef CAKE_TYPES_H
#define CAKE_TYPES_H

#include <cstdint>
#include <type_traits>

namespace cake {
	using size_type = size_t;

	template<typename T>
	struct memory_region {
		T *m_origin;
		size_type m_size;

		memory_region() noexcept: m_origin { nullptr }, m_size { 0 } {}

		memory_region(T *const origin, size_type size) noexcept:
			m_origin { origin },
			m_size { size }
		{
		}

		template<typename U>
		memory_region(const memory_region<U> &other) noexcept:
			m_origin { reinterpret_cast<T *>(other.m_origin) },
			m_size { other.m_size }
		{
		}

		template<typename U>
		memory_region(memory_region<U> &&other) noexcept:
			m_origin { reinterpret_cast<T *>(other.m_origin) },
			m_size { other.m_size }
		{
		}

		template<typename U>
		memory_region &operator=(const memory_region<U> &other) noexcept
		{
			m_origin = other.m_origin;
			m_size = other.m_size;
		}

		template<typename U>
		memory_region &operator=(memory_region<U> &&other) noexcept
		{
			m_origin = other.m_origin;
			m_size = other.m_size;
		}
	};

	using generic_memory_region = memory_region<void>;

	// Sanity checks
	static_assert(std::is_integral<size_type>::value);
}

#endif // CAKE_TYPES_H
