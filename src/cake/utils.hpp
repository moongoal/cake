#ifndef CAKE_UTILS_H
#define CAKE_UTILS_H

#include "types.hpp"

namespace cake::utils {
	template<typename C, typename T, typename... Args>
	using class_fn_ptr = T (C::*)(Args... args);

	/**
	 * Choose either *a* or *b* based on the value of *c*.
	 *
	 * @param a The first value
	 * @param b The second value
	 * @param c The condition
	 *
	 * @return *a* if *c* is true, otherwise *b*
	 */
	template<typename T>
	constexpr T choose(T a, T b, bool c)
	{
		static_assert(sizeof(T) <= 8, "Max size supported is 64 bits");
		static_assert(
			std::is_pointer<T>::value || std::is_unsigned<T>::value,
			"Only valid for pointers or unsigned integers");

		constexpr uint64_t mask = 0xFFFF'FFFF'FFFF'FFFF; // TODO: Convert to use type traits
		uint64_t const x = (uint64_t)a & (mask * c);
		uint64_t const y = (uint64_t)b & (mask * !c);

		return (T)(x + y);
	}

	constexpr size_type min(size_type a, size_type b) noexcept { return choose(a, b, a < b); }

	/**
	 * Returns the arity of a class member function.
	 *
	 * @param fn The member function pointer
	 *
	 * @return An integer representing the arity of the function
	 */
	template<typename C, typename T, typename... Args>
	constexpr size_type get_arity(class_fn_ptr<C, T, Args...> fn) noexcept
	{
		return sizeof...(Args);
	}

	template<typename Arg, typename... Rest>
	constexpr bool is_argument_in_register() noexcept
	{
		constexpr bool arg_res = sizeof(Arg) <= sizeof(uint64_t);

		if constexpr (sizeof...(Rest) == 0) {
			return arg_res;
		} else {
			return arg_res && is_argument_in_register<Rest...>();
		}
	}

	template<typename C, typename T, typename... Args>
	constexpr bool check_all_arguments_in_registers(class_fn_ptr<C, T, Args...> fn) noexcept
	{
		return is_argument_in_register<Args...>();
	}
}

#endif // CAKE_UTILS_H
