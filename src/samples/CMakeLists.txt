add_executable(
    triangle
    WIN32
    triangle/main.cpp
)
set_property(TARGET triangle PROPERTY CXX_STANDARD 17)
target_compile_definitions(
    triangle
    PUBLIC UNICODE
)
target_link_libraries(
    triangle
    PRIVATE cake
)
target_include_directories(
    triangle
    PRIVATE ${PROJECT_SOURCE_DIR}/contrib/glm
)
