#ifndef CAKE_TESTFW_BENCHMARK_H
#define CAKE_TESTFW_BENCHMARK_H

#include <utility>
#define TESTFW_INLINE __attribute__((always_inline))
#define BENCHMARK void testfw::benchmark::benchmark::benchmark_target()

#include <chrono>
#include <vector>
#include <string_view>
#include <filesystem>
#include <fstream>
#include <cstdarg>
#include <iostream>
#include <type_traits>
#include <tuple>

namespace testfw::benchmark {
	template<typename... Ts>
	class benchmark_result {
		template<size_t N>
		using nth_type = typename std::tuple_element<N, std::tuple<Ts...>>::type;

		std::ofstream m_fout;

		template<typename T>
		void write_results(T value)
		{
			m_fout << value << std::endl;
		}

		template<typename T, typename... Args>
		void write_results(T value, Args... other)
		{
			m_fout << value << ",";

			write_results(other...);
		}

		void write_separator(bool is_last)
		{
			if (!is_last) {
				m_fout << ",";
			} else {
				m_fout << std::endl;
			}
		}

		template<typename T>
		void set_single_label(std::string_view label, bool is_last)
		{
			if constexpr (std::is_pod<T>::value) {
				m_fout << label;
			} else {
				constexpr size_t n_entries = std::remove_reference<T>::type::bench_n_entries;

				for (size_t i = 0; i < n_entries - 1; ++i) {
					m_fout << label << "_" << i;
					write_separator(false);
				}

				m_fout << label << "_" << n_entries - 1; // w/o separator
			}

			write_separator(is_last);
		}

		template<size_t I, size_t N_ARGS>
		void set_all_labels(va_list &arg_list)
		{
			if constexpr (I < N_ARGS) {
				constexpr bool is_last = I >= N_ARGS - 1;
				std::string_view label = va_arg(arg_list, const char *);

				set_single_label<nth_type<I>>(label, is_last);
				set_all_labels<I + 1, N_ARGS>(arg_list);
			}
		}

	  public:
		benchmark_result() = delete;
		benchmark_result(const benchmark_result &) = delete;
		benchmark_result(benchmark_result &&) = delete;

		explicit benchmark_result(const std::filesystem::path &file_path): m_fout { file_path }
		{
			std::cout << "Results to: " << file_path << std::endl;
		}

		~benchmark_result()
		{
			m_fout.flush();
			m_fout.close();
		}

		void set_labels(std::string_view label...)
		{
			constexpr size_t n_args = sizeof...(Ts);
			::va_list args;

			va_start(args, label);
			set_single_label<nth_type<0>>(label, n_args == 1);
			set_all_labels<1, n_args>(args);
			va_end(args);
		}

		void add_results(Ts... args) { write_results(args...); }
	};

	class benchmark {
		double m_t_overall;

	  public:
		void benchmark_target();

		void run()
		{
			std::chrono::high_resolution_clock clock;

			auto const t_overall_begin = clock.now();
			benchmark_target();
			auto const t_overall_end = clock.now();

			// Save results
			std::chrono::duration<double> const t_overall_delta = t_overall_end - t_overall_begin;
			m_t_overall = t_overall_delta.count();
		}
	} benchmark_instance;
}

int main()
{
	testfw::benchmark::benchmark_instance.run();

	return 0;
}

#endif // CAKE_TESTFW_BENCHMARK_H
