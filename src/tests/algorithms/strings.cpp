#include <cstring>
#include <testfw/testfw.hpp>
#include <cake/algorithms/strings.hpp>

TEST(value_to_str)
{
	char buff[1024];

	cake::algorithms::value_to_str(buff, 1024, "Hello, ", 155, " world!");

	assert(::strcmp("Hello, 155 world!", buff) == 0);
}
