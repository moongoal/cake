#include "cake/memory/host/stack_allocator.hpp"
#include <utility>
#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory.hpp>
#include <cake/collections/queue.hpp>

using namespace cake;
using namespace cake::memory::host;
using namespace cake::collections;

#define MEM_SIZE (1024 * 1024)
uint8_t memory[MEM_SIZE];

template<typename T>
class test_allocator {
  public:
	using value_type = T;
	using pointer = T *;
	using size_type = cake::size_type;
	using difference_type = cake::size_type;

	stack_allocator *m_stack_allocator;

	explicit test_allocator(stack_allocator *allocator = nullptr): m_stack_allocator { allocator }
	{
	}
	test_allocator(const test_allocator &other) noexcept = default;
	test_allocator(test_allocator &&other) noexcept = default;

	[[nodiscard]] constexpr pointer allocate(size_type n) noexcept
	{
		return reinterpret_cast<pointer>(m_stack_allocator->allocate(sizeof(T) * n, alignof(T)));
	}

	[[nodiscard]] constexpr pointer reallocate(pointer p, size_type n, size_type new_n)
	{
		return reinterpret_cast<pointer>(m_stack_allocator->reallocate(p, sizeof(T) * new_n));
	}

	constexpr void deallocate(pointer p, size_type n) noexcept
	{
		return m_stack_allocator->free(p);
	}
};

template<typename T>
using test_queue = queue<T, test_allocator<T>>;

TEST(test_enqueue)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	queue.enqueue(5);
	assert(queue.size() == 1 && *(queue.begin()) == 5);
}

TEST(test_dequeue)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	queue.enqueue(5);
	int const value = queue.dequeue();
	assert(queue.size() == 0 && value == 5);
}

TEST(test_size)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	assert(queue.size() == 0);

	queue.enqueue(5);
	assert(queue.size() == 1);

	queue.enqueue(10);
	assert(queue.size() == 2);

	queue.enqueue(20);
	assert(queue.size() == 3);

	queue.dequeue();
	assert(queue.size() == 2);

	queue.dequeue();
	assert(queue.size() == 1);

	queue.dequeue();
	assert(queue.size() == 0);
}

TEST(test_peek)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	queue.enqueue(5);

	assert(queue.peek() == 5);
}

TEST(test_copy_ctor)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	queue.enqueue(5);

	test_queue<int> queue2 { queue };

	assert(queue2.size() == 1);
	assert(queue2.dequeue() == 5);
	assert(queue.size() == 1 && queue.peek() == 5);
}

TEST(test_move_ctor)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &stack_allocator };
	test_queue<int> queue { 16, allocator };

	queue.enqueue(5);

	test_queue<int> queue2 { std::move(queue) };

	assert(queue2.size() == 1);
	assert(queue2.dequeue() == 5);
}
