#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory.hpp>
#include <cake/collections/vector.hpp>

using namespace cake;
using namespace cake::memory::host;
using namespace cake::collections;

#define CTOR_VOID 1
#define CTOR_REF 2
#define CTOR_MOVE 3
#define DTOR_CALLED 4

#define MEM_SIZE (1024 * 1024)
uint8_t memory[MEM_SIZE];

template<typename T>
class test_allocator {
  public:
	using value_type = T;
	using pointer = T *;
	using size_type = cake::size_type;

	arena_allocator<PAGE_SIZE_SMALL> *m_arena_allocator;

	explicit test_allocator(arena_allocator<PAGE_SIZE_SMALL> *allocator = nullptr) noexcept:
		m_arena_allocator { allocator }
	{
	}
	test_allocator(const test_allocator &other) noexcept = default;
	test_allocator(test_allocator &&other) noexcept = default;

	[[nodiscard]] constexpr pointer allocate(size_type n) noexcept
	{
		return reinterpret_cast<pointer>(m_arena_allocator->allocate(sizeof(T) * n, alignof(T)));
	}

	constexpr void deallocate(pointer p, size_type n) noexcept
	{
		return m_arena_allocator->free(p);
	}

	[[nodiscard]] constexpr pointer reallocate(pointer p, size_type n, size_type new_n)
	{
		return reinterpret_cast<pointer>(m_arena_allocator->reallocate(p, sizeof(T) * new_n));
	}
};

template<typename T>
using Vec = vector<T, test_allocator<T>>;

struct C {
	int x;

	C(): x(CTOR_VOID) {}
	C(const C &other): x(CTOR_REF) {}
	C(C &&other): x(CTOR_MOVE) {}

	~C() { x = DTOR_CALLED; }
};

TEST(test_push_back)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	Vec<int> v { 32, allocator };

	v.push_back(10);
	assert(v.size() == 1);

	v.push_back(7);
	assert(v.size() == 2);
	assert(*v.data() == 10);
	assert(*(v.data() + 1) == 7);

	test_allocator<C> allocator2 { &arena_allocator };
	Vec<C> v2 { 5, allocator2 };

	// Move constructor
	v2.push_back(C());
	assert(v2[0].x == CTOR_MOVE);

	// Reference constructor
	C c {};
	v2.push_back(c);
	assert(v2[1].x == CTOR_REF);
}

TEST(test_pop)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	Vec<int> v { 32, allocator };

	v.push_back(10);
	v.push_back(7);

	int a = v.back();
	v.pop();
	assert(v.size() == 1);

	int b = v.back();
	v.pop();
	assert(v.size() == 0);

	assert(a == 7);
	assert(b == 10);

	test_allocator<C> allocator2 { &arena_allocator };
	Vec<C> v2 { 5, allocator2 };

	// Destructor
	v2.push_back(C());
	C *c_ptr = v2.data();

	assert(c_ptr->x == CTOR_MOVE);
	C &ret_c = v2.back();

	v2.pop();
	assert(ret_c.x == DTOR_CALLED);
}

TEST(test_size)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	Vec<int> v(32, allocator);

	assert(v.size() == 0);

	v.push_back(2);
	assert(v.size() == 1);
}

TEST(test_capacity)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	Vec<int> v(32, allocator);

	assert(v.capacity() == 32);
}

TEST(test_data)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	auto v = Vec<int>(32, allocator);

	v.push_back(5);
	v.push_back(10);

	assert(*v.data() == 5);
	assert(*(v.data() + 1) == 10);
}

TEST(test_remove)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	auto v = Vec<int>(32, allocator);

	v.push_back(5);
	v.push_back(10);
	assert(v.size() == 2);

	v.remove(1);
	assert(v.size() == 1);

	v.remove(0);
	assert(v.size() == 0);
}

TEST(test_operator_subscript)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	auto v = Vec<int>(32, allocator);

	v.push_back(5);
	v.push_back(10);
	assert(v.size() == 2);

	v.remove(1);
	assert(v.size() == 1);

	v.remove(0);
	assert(v.size() == 0);
}

TEST(test_back)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	auto v = Vec<int>(32, allocator);

	v.push_back(5);
	assert(v[v.size() - 1] == 5);
	v.push_back(10);
	assert(v[v.size() - 1] == 10);
	v.remove(0);
	assert(v[v.size() - 1] == 10);
}

TEST(test_insert)
{
	arena_allocator<PAGE_SIZE_SMALL> arena_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	test_allocator<int> allocator { &arena_allocator };
	auto v = Vec<int>(3, allocator);

	v.push_back(5);
	v.push_back(10);
	v.insert(1, 6);

	assert(v.size() == 3);
	assert(v[0] == 5);
	assert(v[1] == 6);
	assert(v[2] == 10);

	v.insert(0, 100);

	assert(v.size() == 4);
	assert(v[0] == 100);
	assert(v[1] == 5);
	assert(v[2] == 6);
	assert(v[3] == 10);

	v.insert(v.size() - 1, 666);
	assert(v.size() == 5);
	assert(v[0] == 100);
	assert(v[1] == 5);
	assert(v[2] == 6);
	assert(v[3] == 666);
	assert(v[4] == 10);
}
