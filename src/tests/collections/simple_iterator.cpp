#include <cstdint>
#include <algorithm>
#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory.hpp>
#include <cake/collections/vector.hpp>

using namespace cake;
using namespace cake::memory::host;
using namespace cake::collections;

#define MEM_SIZE (1024 * 1024)
uint8_t memory[MEM_SIZE];

template<typename T>
class test_allocator {
  public:
	using value_type = T;
	using pointer = T *;
	using size_type = cake::size_type;
	using difference_type = cake::size_type;

	stack_allocator *m_stack_allocator;

	explicit test_allocator(stack_allocator *allocator = nullptr): m_stack_allocator { allocator }
	{
	}
	test_allocator(const test_allocator &other) noexcept = default;
	test_allocator(test_allocator &&other) noexcept = default;

	[[nodiscard]] constexpr pointer allocate(size_type n) noexcept
	{
		return reinterpret_cast<pointer>(m_stack_allocator->allocate(sizeof(T) * n, alignof(T)));
	}

	[[nodiscard]] constexpr pointer reallocate(pointer p, size_type n, size_type new_n)
	{
		return reinterpret_cast<pointer>(m_stack_allocator->reallocate(p, sizeof(T) * new_n));
	}

	constexpr void deallocate(pointer p, size_type n) noexcept
	{
		return m_stack_allocator->free(p);
	}
};

template<typename T>
using Vec = vector<T, test_allocator<T>>;

TEST(test_iter_forward)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	Vec<int> v { 16, test_allocator<int> { &stack_allocator } };

	v.reserve(4);
	v.push_back(5);
	v.push_back(10);
	v.push_back(50);
	v.push_back(500);

	int values[] = { 5, 10, 50, 500 };

	const Vec<int>::iterator vend = v.cend();
	size_type i = 0;

	for (Vec<int>::iterator it = v.begin(); it != vend; it++) { assert(*it == values[i++]); }

	assert(i == 4);
}

TEST(test_iter_backward)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	Vec<int> v { 16, test_allocator<int> { &stack_allocator } };

	v.reserve(4);
	v.push_back(5);
	v.push_back(10);
	v.push_back(50);
	v.push_back(500);

	int values[] = { 5, 10, 50, 500 };

	const Vec<int>::iterator vend = v.cbegin();
	size_type i = 3;

	for (Vec<int>::iterator it = v.end() - 1; it >= vend; it--) { assert(*it == values[i--]); }

	assert(i == -1);
}

TEST(test_operators)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	Vec<int> v { 16, test_allocator<int> { &stack_allocator } };

	v.reserve(4);
	v.push_back(5);
	v.push_back(10);

	Vec<int>::iterator it1 = v.begin();
	Vec<int>::iterator it2 = v.begin();
	Vec<int>::iterator it3 = v.begin();

	// Postfix increment
	assert(*(it2++) == 5);

	// Postfix decrement
	assert(*(it2--) == 10);

	// Prefix increment
	assert(*(++it2) == 10);

	// Prefix decrement
	assert(*(--it2) == 5);

	// Comparison
	it2++;
	assert(it1 < it2);
	assert(it1 <= it2);
	assert(it1 == it3);
	assert(it2 == it2);
	assert(it1 != it2);
	assert(it2 > it1);
	assert(it2 >= it1);

	// Arithmetic
	assert(it2 - it1 == 1);
	assert(it3 - it1 == 0);
	assert(it1 - it2 == -1);

	// Subscript
	assert(it1[0] == 5);
	assert(it1[1] == 10);
	assert(it2[0] == 10);
	assert(it2[-1] == 5);
}

TEST(test_member_access_operator)
{
	struct TestStruct {
		int a;
		int b;
	};

	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	Vec<TestStruct> v { 6, test_allocator<TestStruct> { &stack_allocator } };

	TestStruct t1;
	t1.a = 50;
	t1.b = -100;

	v.push_back(t1);
	Vec<TestStruct>::iterator it = v.begin();

	assert(it->a == 50);
	assert(it->b == -100);
}

TEST(test_stdlib_sort)
{
	stack_allocator stack_allocator { generic_memory_region { ::memory, MEM_SIZE } };
	Vec<int> v { 16, test_allocator<int> { &stack_allocator } };

	v.push_back(12);
	v.push_back(-199);
	v.push_back(5);
	v.push_back(0);
	v.push_back(0);
	v.push_back(2);

	int expected[] = { -199, 0, 0, 2, 5, 12 };

	Vec<int>::iterator begin = v.begin();
	Vec<int>::iterator end = v.end();

	std::sort(begin, end);

	for (size_type i = 0; i < v.size(); i++) { assert(v[i] == expected[i]); }
}
