#include <testfw/testfw.hpp>
#include <cake/concurrent/thread.hpp>
#include <cake/concurrent/spin_lock.hpp>

using namespace cake::concurrent;

struct test_data {
	volatile spin_lock lock;
	volatile bool complete = false;
};

void lock_handler(void *param) noexcept
{
	test_data *tdata = reinterpret_cast<test_data *>(param);

	tdata->lock.acquire();
	tdata->complete = true;
	tdata->lock.release();
}

TEST(acquire_release_st)
{
	spin_lock lock;

	assert2(!lock.is_acquired(), "Invalid initial state");
	lock.acquire();
	assert2(lock.is_acquired(), "Not acquired");
	lock.release();
	assert2(!lock.is_acquired(), "Not released");
}

TEST(is_acquired)
{
	spin_lock lock;
	assert(!lock.is_acquired());

	lock.acquire();
	assert(lock.is_acquired());

	lock.release();
	assert(!lock.is_acquired());
}

TEST(ctor)
{
	spin_lock lock_released, lock_acquired;

	lock_acquired.acquire();
	assert(!lock_released.is_acquired());
	assert(lock_acquired.is_acquired());
}

TEST(acquire_release_mt)
{
	test_data tdata;
	thread t { &lock_handler };

	tdata.lock.acquire();
	t.start(&tdata);
	sleep(100);
	assert2(!tdata.complete, "Lock was acquired twice");
	tdata.lock.release();
	sleep(100);
	assert2(tdata.complete, "Lock wasn't released");
}

void try_acquire_handler(void *param) noexcept
{
	test_data *tdata = reinterpret_cast<test_data *>(param);

	if (!tdata->lock.try_acquire(100)) { tdata->complete = true; }
}

TEST(try_acquire)
{
	test_data tdata;
	thread t { &try_acquire_handler };

	tdata.lock.acquire();
	t.start(&tdata);
	t.join();

	assert(tdata.complete);
}
