#include <atomic>
#include <utility>
#include <chrono>
#include <emmintrin.h>
#include <mmintrin.h>
#include <testfw/testfw.hpp>
#include <cake/concurrent/thread.hpp>
#include <cake/concurrent/thread_guard.hpp>

using namespace cake::concurrent;

void run_handler(void *param) noexcept
{
	std::atomic<int> *x = reinterpret_cast<std::atomic<int> *>(param);

	x->store(1);
}

TEST(start)
{
	using duration = std::chrono::duration<double>;
	constexpr int timeout = 5;

	std::atomic<int> x = 0;
	thread t { &::run_handler };
	thread_guard t_guard { t };

	auto t_begin = std::chrono::steady_clock::now();

	t.start(&x);

	while (!x.load()) {
		auto t_end = std::chrono::steady_clock::now();
		duration tdelta = t_end - t_begin;

		assert(tdelta.count() < timeout);
		_mm_pause();
	}
}

TEST(operator_assign)
{
	thread t { &run_handler };
	thread t2;

	t2 = std::move(t);

	assert(t2.get_handler() == &::run_handler);
	assert(t2.get_state() == thread_state::created);
	assert(t.get_state() == thread_state::invalid);
}

TEST(ctor_move)
{
	const char * const thr_name = "My super thread";
	thread t { &run_handler, thr_name };
	thread t2 { std::move(t) };

	assert(t2.get_handler() == &::run_handler);
	assert(t2.get_state() == thread_state::created);
	assert(t2.get_name() == thr_name);
	assert(t.get_state() == thread_state::invalid);
	assert(t.get_name() == thr_name);
}

TEST(ctor_default)
{
	thread t;

	assert(t.get_state() == thread_state::invalid);
	assert(t.get_handler() == nullptr);
	assert(t.get_id() == INVALID_THREAD_ID);
	assert(t.get_name() == UNNAMED_THREAD_NAME);
}

void join_handler(void *param) noexcept { cake::concurrent::sleep(1000); }

TEST(join)
{
	thread t { &join_handler };

	auto t_begin = std::chrono::steady_clock::now();
	t.start(nullptr);
	t.join();
	auto t_end = std::chrono::steady_clock::now();
	std::chrono::duration<double> t_delta = t_end - t_begin;

	assert(t_delta.count() >= 1 && t_delta.count() < 10);
}

TEST(try_join)
{
	thread t { &join_handler };

	t.start(nullptr);
	bool const should_fail = t.try_join(10);
	assert2(!should_fail, "Didn't fail where it should have");

	bool const should_succeed = t.try_join(1200);
	assert2(should_succeed, "Didn't succeed where it should have");
}
