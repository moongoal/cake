#include <testfw/testfw.hpp>
#include <cake/memory/host/arena_allocator.hpp>

#define TEST_MEM_SIZE (1024 * 1024 * 3)

uint8_t memory[TEST_MEM_SIZE];

using namespace cake;
using namespace cake::memory::host;

using test_arena_allocator = arena_allocator<cake::memory::host::PAGE_SIZE_SMALL>;

TEST(allocate)
{
	test_arena_allocator allocator { generic_memory_region { ::memory,
															 TEST_MEM_SIZE * sizeof(uint8_t) } };

	uint8_t *int_ptr_allocated =
		reinterpret_cast<uint8_t *>(allocator.allocate(sizeof(int), alignof(int)));

	assert(int_ptr_allocated >= ::memory && int_ptr_allocated < ::memory + TEST_MEM_SIZE);
}

TEST(clear)
{
	test_arena_allocator allocator { generic_memory_region { ::memory,
															 TEST_MEM_SIZE * sizeof(uint8_t) } };

	void *const ptr1 = allocator.allocate(sizeof(int), alignof(int));
	allocator.clear();
	void *const ptr2 = allocator.allocate(sizeof(int), alignof(int));

	assert(ptr1 == ptr2);
}

TEST(reallocate_grow)
{
	test_arena_allocator allocator { generic_memory_region { ::memory,
															 TEST_MEM_SIZE * sizeof(uint8_t) } };

	int *int_ptr_allocated = reinterpret_cast<int *>(allocator.allocate(sizeof(int), alignof(int)));
	int *int_ptr_allocated2 =
		reinterpret_cast<int *>(allocator.allocate(sizeof(int), alignof(int)));

	void *int_ptr_reallocated2 = allocator.reallocate(int_ptr_allocated2, sizeof(int) * 2);

	assert(int_ptr_reallocated2 == int_ptr_allocated2);
}

TEST(reallocate_relocate)
{
	test_arena_allocator allocator { generic_memory_region { ::memory,
															 TEST_MEM_SIZE * sizeof(uint8_t) } };

	uint32_t *int_ptr_allocated =
		reinterpret_cast<uint32_t *>(allocator.allocate(sizeof(uint32_t), alignof(uint32_t)));
	*int_ptr_allocated = 0xabab;

	uint32_t *int_ptr_allocated2 =
		reinterpret_cast<uint32_t *>(allocator.allocate(sizeof(uint32_t), alignof(uint32_t)));
	*int_ptr_allocated2 = 0xcdcd;

	uint32_t *int_ptr_reallocated = reinterpret_cast<uint32_t *>(
		allocator.reallocate(int_ptr_allocated, sizeof(uint32_t) * 1024 * 5));
	*int_ptr_reallocated = 0xefef;

	assert(
		int_ptr_reallocated > int_ptr_allocated2 + 1
		&& reinterpret_cast<uint8_t *>(int_ptr_reallocated) < ::memory + TEST_MEM_SIZE);

	assert(*int_ptr_allocated2 == 0xcdcd);
	assert(*int_ptr_reallocated == 0xefef);
}

TEST(reallocate_shrink)
{
	test_arena_allocator allocator { generic_memory_region { ::memory,
															 TEST_MEM_SIZE * sizeof(uint8_t) } };

	int *int_ptr_allocated =
		reinterpret_cast<int *>(allocator.allocate(sizeof(int) * 2, alignof(int)));
	int *int_ptr_allocated2 =
		reinterpret_cast<int *>(allocator.allocate(sizeof(int) * 2, alignof(int)));

	void *int_ptr_reallocated2 = allocator.reallocate(int_ptr_allocated2, sizeof(int) * 2);
	void *int_ptr_reallocated = allocator.reallocate(int_ptr_allocated, sizeof(int));

	assert(int_ptr_reallocated2 == int_ptr_allocated2);
	assert(int_ptr_reallocated == int_ptr_allocated);
}
