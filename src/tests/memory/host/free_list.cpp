#include <cstdint>
#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory/host/free_list.hpp>

using namespace cake;
using namespace cake::memory::host;

#define MEMORY_SZ (1024 * 1024)
#define PAGE_SIZE 256
uint8_t memory[MEMORY_SZ];

TEST(ctor)
{
	free_list list;

	assert(list.get_first_node() == nullptr);
}

TEST(add_node_and_coalesce)
{
	free_list list;

	uint8_t *const ptr1 = ::memory;
	free_list::node_pointer node1 =
		list.add_node(free_list::memory_region_type { ptr1, PAGE_SIZE });

	uint8_t *const ptr2 = ptr1 + 3 * PAGE_SIZE;
	free_list::node_pointer node2 =
		list.add_node(free_list::memory_region_type { ptr2, PAGE_SIZE });

	uint8_t *const ptr3 = ptr2 + 2 * PAGE_SIZE;
	free_list::node_pointer node3 =
		list.add_node(free_list::memory_region_type { ptr3, PAGE_SIZE });

	uint8_t *const ptr4 = ptr3 + PAGE_SIZE; // Right-adjacent to node3
	free_list::node_pointer node4 =
		list.add_node(free_list::memory_region_type { ptr4, PAGE_SIZE });

	assert(list.get_first_node() == node1);

	// Test created nodes
	assert(list.find_previous_node(*node1) == nullptr);
	assert(node1->get_next() == node2);
	assert(node1->get_ptr() == ptr1);
	assert(node1->get_size() == PAGE_SIZE);

	assert(list.find_previous_node(*node2) == node1);
	assert(node2->get_next() == node3);
	assert(node2->get_ptr() == ptr2);
	assert(node2->get_size() == PAGE_SIZE);

	assert(list.find_previous_node(*node3) == node2);
	assert(node3->has_next() == false);
	assert(node3->get_ptr() == ptr3);
	assert(node3->get_size() == 2 * PAGE_SIZE);

	assert(node4 == node3); // Coalesce with previous

	uint8_t *const ptr5 = ptr4 + 3 * PAGE_SIZE;
	free_list::node_pointer node5 =
		list.add_node(free_list::memory_region_type { ptr5, PAGE_SIZE });

	uint8_t *const ptr6 = ptr5 - PAGE_SIZE; // Left-adjacent to node5
	free_list::node_pointer node6 =
		list.add_node(free_list::memory_region_type { ptr6, PAGE_SIZE });

	assert(node3->get_next() == node6);			// Insert in-between
	assert(node6->get_size() == 2 * PAGE_SIZE); // Coalesce with next
	assert(node6->has_next() == false);
	assert(list.find_previous_node(*node6) == node3);
}

TEST(add_prepend)
{
	free_list list;

	uint8_t *const ptr1 = ::memory + 3 * PAGE_SIZE;
	free_list::node_pointer node1 =
		list.add_node(free_list::memory_region_type { ptr1, PAGE_SIZE });

	uint8_t *const ptr2 = ::memory;
	free_list::node_pointer node2 =
		list.add_node(free_list::memory_region_type { ptr2, PAGE_SIZE });

	// node2 has to be prepended due to lower memory address
	assert(list.get_first_node() == node2);
	assert(node2->get_next() == node1);
	assert(list.find_previous_node(*node2) == nullptr);

	assert(node1->get_next() == nullptr);
	assert(list.find_previous_node(*node1) == node2);
}

TEST(remove_node)
{
	free_list list;

	uint8_t *const ptr1 = ::memory;
	free_list::node_pointer node1 =
		list.add_node(free_list::memory_region_type { ptr1, PAGE_SIZE });

	uint8_t *const ptr2 = ptr1 + 3 * PAGE_SIZE;
	free_list::node_pointer node2 =
		list.add_node(free_list::memory_region_type { ptr2, PAGE_SIZE });

	uint8_t *const ptr3 = ptr2 + 2 * PAGE_SIZE;
	free_list::node_pointer node3 =
		list.add_node(free_list::memory_region_type { ptr3, PAGE_SIZE });

	list.remove_node(*node2);
	assert(node1->get_next() == node3);
	assert(list.find_previous_node(*node3) == node1);

	list.remove_node(*node1);
	assert(!list.find_previous_node(*node3));
	assert(!node3->has_next());

	list.remove_node(*node3);
	assert(list.get_first_node() == nullptr);
}

TEST(find_node_by_size)
{
	free_list list;

	uint8_t *const ptr1 = ::memory;
	free_list::node_pointer node1 =
		list.add_node(free_list::memory_region_type { ptr1, PAGE_SIZE });

	uint8_t *const ptr2 = ptr1 + 3 * PAGE_SIZE;
	free_list::node_pointer node2 =
		list.add_node(free_list::memory_region_type { ptr2, 2 * PAGE_SIZE });

	free_list::find_node_result result;

	assert(list.find_node_by_size(PAGE_SIZE / 2, &result) && result.m_node == node1);
	assert(list.find_node_by_size(PAGE_SIZE, &result) && result.m_node == node1);
	assert(list.find_node_by_size(PAGE_SIZE * 2, &result) && result.m_node == node2);
	assert(!list.find_node_by_size(PAGE_SIZE * 3, &result));
}

TEST(reduce_size)
{
	free_list list;

	uint8_t *const ptr1 = ::memory;
	free_list::node_pointer node1 =
		list.add_node(free_list::memory_region_type { ptr1, PAGE_SIZE });
	void *const x = list.reduce_size(&node1, 6);
	node1 = list.get_first_node();

	assert(x == ptr1);
	assert(node1->get_size() == PAGE_SIZE - 6);
	assert(reinterpret_cast<uint8_t *>(node1) == ptr1 + 6);

	list.reduce_size(&node1, PAGE_SIZE - 6);

	assert(list.get_first_node() == node1 && node1 == nullptr);
}
