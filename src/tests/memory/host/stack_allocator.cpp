#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory/host/stack_allocator.hpp>

#define TEST_MEM_SIZE 1024

using namespace cake;
using namespace cake::memory::host;

TEST(allocate)
{
	uint8_t memory[TEST_MEM_SIZE];
	stack_allocator allocator { generic_memory_region { memory, TEST_MEM_SIZE * sizeof(uint8_t) } };

	uint8_t *ptr_allocated = reinterpret_cast<uint8_t *>(reinterpret_cast<int*>(allocator.allocate(sizeof(int), alignof(int))));

	assert(ptr_allocated == memory + sizeof(stack_allocator::alloc_header));
}

TEST(deallocate)
{
	uint8_t memory[TEST_MEM_SIZE];
	stack_allocator allocator { generic_memory_region { memory, TEST_MEM_SIZE * sizeof(uint8_t) } };

	void *base_ptr = allocator.get_base();
	assert(allocator.get_head() == base_ptr);

	int *alloc_ptr1 = reinterpret_cast<int*>(allocator.allocate(sizeof(int), alignof(int)));
	uint16_t *alloc_ptr2 = reinterpret_cast<uint16_t*>(allocator.allocate(sizeof(uint16_t), alignof(uint16_t)));
	uint8_t *alloc_ptr3 = reinterpret_cast<uint8_t*>(allocator.allocate(sizeof(uint8_t), alignof(uint8_t)));
	assert(allocator.get_head() > base_ptr);

	allocator.free(alloc_ptr3);
	allocator.free(alloc_ptr2);
	allocator.free(alloc_ptr1);
	assert(allocator.get_head() == base_ptr);
}

TEST(clear)
{
	uint8_t memory[TEST_MEM_SIZE];
	stack_allocator allocator { generic_memory_region { memory, TEST_MEM_SIZE * sizeof(uint8_t) } };

	allocator.allocate(sizeof(int), alignof(int));
	assert(allocator.get_head() > allocator.get_base());

	allocator.clear();
	assert(allocator.get_head() == allocator.get_base());
}

TEST(reallocate_grow)
{
	uint8_t memory[TEST_MEM_SIZE];
	stack_allocator allocator { generic_memory_region { memory, TEST_MEM_SIZE * sizeof(uint8_t) } };

	int *int_ptr_allocated = reinterpret_cast<int*>(allocator.allocate(sizeof(int), alignof(int)));
	int *int_ptr_allocated2 = reinterpret_cast<int*>(allocator.allocate(sizeof(int), alignof(int)));

	void *int_ptr_reallocated2 = allocator.reallocate(int_ptr_allocated2, sizeof(int) * 2);

	assert(int_ptr_reallocated2 == int_ptr_allocated2);
}

TEST(reallocate_shrink)
{
	uint8_t memory[TEST_MEM_SIZE];
	stack_allocator allocator { generic_memory_region { memory, TEST_MEM_SIZE * sizeof(uint8_t) } };

	int *int_ptr_allocated = reinterpret_cast<int*>(allocator.allocate(sizeof(int) * 2, alignof(int)));
	int *int_ptr_allocated2 = reinterpret_cast<int*>(allocator.allocate(sizeof(int) * 2, alignof(int)));

	void *int_ptr_reallocated2 = allocator.reallocate(int_ptr_allocated2, sizeof(int) * 2);
	void *int_ptr_reallocated = allocator.reallocate(int_ptr_allocated, sizeof(int));

	assert(int_ptr_reallocated2 == int_ptr_allocated2);
	assert(int_ptr_reallocated == int_ptr_allocated);
}
