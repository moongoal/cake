#include "cake/memory/host/subsystem.hpp"
#include <testfw/testfw.hpp>
#include <cake/types.hpp>
#include <cake/memory.hpp>

using namespace cake;
using namespace cake::memory::host;

constexpr size_type sh_mem_sz = 1024 * 1024 * 16; // 16 MiB
constexpr size_type lh_mem_sz = 1024 * 1024 * 16; // 16 MiB

SETUP
{
	memory::host::configuration cfg { sh_mem_sz, lh_mem_sz };
	memory::host::startup(cfg);
}

CLEANUP { memory::host::shutdown(); }

TEST(alloc_small)
{
	allocator<int> allocator;
	constexpr int n = 1;
	int *x = allocator.allocate(n);

	assert(x);
	assert(
		get_small_heap_allocator().get_free_memory()
		< get_small_heap_allocator().get_memory_size());
	assert(
		get_large_heap_allocator().get_free_memory()
		== get_large_heap_allocator().get_memory_size());

	allocator.deallocate(x, n);
}

TEST(alloc_large)
{
	allocator<int> allocator;
	constexpr int n = 1024 * 1024;
	int *x = allocator.allocate(n);

	assert(x);
	assert(
		get_small_heap_allocator().get_free_memory()
		== get_small_heap_allocator().get_memory_size());
	assert(
		get_large_heap_allocator().get_free_memory()
		< get_large_heap_allocator().get_memory_size());

	allocator.deallocate(x, n);
}
