#include <testfw/testfw.hpp>
#include <cake/utils.hpp>

using namespace cake;

TEST(choose)
{
	// Unsigned integer value
	assert(utils::choose(10u, 11u, 3 < 5) == 10);
	assert(utils::choose(7u, 13u, 3 != 3) == 13);

	// Pointer value
	uint8_t *a = reinterpret_cast<uint8_t *>(0xabab);
	uint8_t *b = reinterpret_cast<uint8_t *>(0xbcbc);

	assert(utils::choose(a, b, a == b) == b);
}
